package com.neziaa.core.utils.json;

import java.io.File;

import com.neziaa.core.Main;

import net.minecraft.util.com.google.gson.Gson;

public interface JsonPersist {
	
	public Gson gson = (Main.getInstance().getGson());
	
	public File getFile();
	
	public void loadData();
	
	public void saveData(boolean sync);
	

}
