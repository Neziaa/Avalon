package com.neziaa.core.utils;

public interface Saveable {
	
	public void load();
	
	public void save(boolean value);

}
