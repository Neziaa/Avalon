package com.neziaa.core;

import java.lang.reflect.Modifier;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import com.google.common.collect.Lists;
import com.neziaa.core.command.CommandFramework;
import com.neziaa.core.command.commands.PetCommand;
import com.neziaa.core.configuration.Configuration;
import com.neziaa.core.configuration. ConfigurationManager;
import com.neziaa.core.pet.PetManager;
import com.neziaa.core.utils.Saveable;
import com.neziaa.core.utils.json.ItemStackAdapter;
import com.neziaa.core.utils.json.JsonPersist;
import com.neziaa.core.utils.json.LocationAdapter;
import com.neziaa.core.utils.json.PotionEffectAdapter;
import com.neziaa.core.utils.sql.MySQL;

import lombok.Getter;
import net.minecraft.util.com.google.gson.Gson;
import net.minecraft.util.com.google.gson.GsonBuilder;

public class Main extends JavaPlugin {
	
	@Getter private static Main instance;
	@Getter private Gson gson;
	@Getter private CommandFramework framework;
	
	@Getter private static Configuration configuration;
	@Getter private static ConfigurationManager configurationManager;
	
	@Getter private static PetManager petManager;

	@Getter private List<JsonPersist> persistances = Lists.newArrayList();
	@Getter private List<Saveable> saveables = Lists.newArrayList();
	
	public void onEnable() {
		instance = this;
		this.getDataFolder().mkdir();
		this.framework = new CommandFramework(this);
		this.gson = this.getGsonBuilder().create();
		
		petManager = new PetManager();
		
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerListeners(), this);
		
		this.framework.registerCommands(new PetCommand());
		
		this.persistances.add(petManager);
		this.persistances.forEach(p -> p.loadData());
		
		this.saveables.add(configurationManager);
		this.saveables.forEach(p -> p.load());
		
		configurationManager = new ConfigurationManager();
		configuration = configurationManager.getConfiguration();
		
		new MySQL().connect(configuration.getHost(),configuration.getPort(), configuration.getDatabase(), configuration.getUser(), configuration.getPass());
	}
	
	public void onDisable() {
		this.persistances.forEach(p -> p.saveData(true));
		this.saveables.forEach(p -> p.save(true));
		MySQL.i.closeConnection();
	}
	
	private GsonBuilder getGsonBuilder() {
        return new GsonBuilder()
        		.setPrettyPrinting()
        		.disableHtmlEscaping()
        		.serializeNulls()
        		.excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE)
        		.registerTypeHierarchyAdapter(ItemStack.class, new ItemStackAdapter())
        		.registerTypeAdapter(PotionEffect.class, new PotionEffectAdapter())
        		.registerTypeAdapter(Location.class, new LocationAdapter());
    }
}
