package com.neziaa.core.command.commands;

import com.neziaa.core.Main;
import com.neziaa.core.command.Command;
import com.neziaa.core.command.CommandArgs;
import com.neziaa.core.pet.Pet;
import com.neziaa.core.pet.PetEnum;
import com.neziaa.core.utils.Utils;
public class PetCommand {
	
	@Command(name = "pet.create",
			 permission = "core.pet",
			 description = "",
			 usage = "/pet",
			 inGameOnly = true)
	public void onCommand(CommandArgs args) {
		Pet pet = new Pet(args.getSender().getName(), PetEnum.SUPPORT);
		pet.create();
	}
	
	@Command(name = "pet",
			 permission = "core.pet",
			 description = "",
			 usage = "/pet",
			 inGameOnly = true)
	public void cmd(CommandArgs args) {
		for(Pet pet: Main.getPetManager().getPets()) {
			args.getSender().sendMessage(Utils.color("&bPET : " + pet.getName() + " appartient � " + pet.getOwner() + " type : " + pet.getType().toString()));
		}
	}
	
	
	
	

}
