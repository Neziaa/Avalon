package com.neziaa.core.configuration;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.Bukkit;

import com.neziaa.core.Main;
import com.neziaa.core.utils.Saveable;
import com.neziaa.core.utils.json.DiscUtil;

import net.minecraft.util.com.google.gson.reflect.TypeToken;

public class ConfigurationManager implements Saveable {
	
	private Configuration configuration;
	private TypeToken<Configuration> type = new TypeToken<Configuration>() {};

	public File getFile() {
		return new File(Main.getInstance().getDataFolder(), "configuration.json");
	}
	
	@Override
	public void load() {
		String content = DiscUtil.readCatch(this.getFile());
		
		if (content == null) {
			return;
		}
		
		try {
			this.configuration = Main.getInstance().getGson().fromJson(content, type.getType());
		} catch (Exception ex) {
			Bukkit.getLogger().log(Level.SEVERE, "Probl�me de configuration... " + ex);
		}
	}

	@Override
	public void save(boolean async) {
		DiscUtil.writeCatch(getFile(), Main.getInstance().getGson().toJson(this.getConfiguration()), true);
	}
	
	public Configuration getConfiguration() {
		if (this.configuration == null)
			return this.configuration = new Configuration();
		return this.configuration;
	}

}
