package com.neziaa.core.configuration;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
public class Configuration {
	
	private String host = "localhost";
	private String port = "3306";
	private String database  = "avalon";
	private String user = "root";
	private String pass = "toor";
	
	/** --- **/
}
