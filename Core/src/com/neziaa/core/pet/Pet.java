package com.neziaa.core.pet;

import org.bukkit.Bukkit;

import com.neziaa.core.Main;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString 
public class Pet {
	
	private Integer id;
	private String owner;
	private Long time;
	
	private String name;
	private Integer level;
	private PetEnum type;
	private Integer life;
	
	public Pet(String owner, PetEnum type) {
		this.id = Main.getPetManager().getPets().size() + 1;
		this.owner = owner;
		this.time = System.currentTimeMillis();
		
		this.name = "Luc";
		this.level = 0;
		this.type = type;
		this.life = 20;
	}
	
	public void create() {
		Main.getPetManager().getPets().add(this);
		Bukkit.getConsoleSender().sendMessage("�aLe pet " + name + " vient d'�tre cr�er!");
	}
	
	
}
