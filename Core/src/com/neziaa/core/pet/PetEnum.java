package com.neziaa.core.pet;

import org.bukkit.Material;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter @AllArgsConstructor
public enum PetEnum {
	
	SUPPORT("Support", Material.EGG, 500),
	
	TANK("Tank", Material.EGG, 500),
	
	REVANCHARD("Revanchard", Material.EGG, 500),
	
	COMBATTANT("Combattant", Material.EGG, 500);
	
	private String name; 
	private Material oeuf; 
	private Integer prix; 

}
