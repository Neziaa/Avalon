package com.neziaa.core.pet;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.neziaa.core.Main;
import com.neziaa.core.utils.ItemBuilder;
import com.neziaa.core.utils.Utils;

public class PetInventory {
	
	public void mainInventory(Player p){
		
		Inventory inv = Bukkit.createInventory(null, 27, Utils.color("�bMenu des familliers"));
		p.openInventory(inv);
		
		 ItemStack shop = new ItemBuilder(Material.GOLD_INGOT).setName("�eAchat des familliers").setLore("�7Cliquer pour acheter des familliers").toItemStack();
		 ItemStack stats = new ItemBuilder(Material.NETHER_STAR).setName(Utils.color("�b" + p.getName())).setLore("�7Vos statistique ").toItemStack();
		 ItemStack petlist = new ItemBuilder(Material.SKULL).setName(Utils.color("�bVos familliers")).toItemStack();
		 	 
		 inv.setItem(12, shop);
		 inv.setItem(14, stats);
		 inv.setItem(16, petlist);
		 
	}
	
	public void PetShopInventory(Player p){
		
		Inventory inv = Bukkit.createInventory(null, 27, "�9Pets - Achats");
		p.openInventory(inv);
		
		for(PetEnum pets: PetEnum.values()) {
			int count = 11;
			inv.setItem(count, new ItemBuilder(pets.getOeuf()).setName(pets.getName()).setLore("�aPrix : " + pets.getPrix()).toItemStack()); 
			
			/**
			 * TODO: link with Base economy 
			 * -> pets.isPossibleToBuy() ? "�c", "�a")
			 */
			count++;
		}
		
		return;
	}
	
	public void PetInventory(Player p){
		
		Inventory inv = Bukkit.createInventory(null, 27, Utils.color("�bVos familliers"));
		p.openInventory(inv);
		
		ArrayList<Pet> list = Main.getPetManager().getPets(p.getName());
		
		if(list.size() > 0) {
			for(Pet pet: list) {
				int count = 11;
				inv.setItem(count, new ItemBuilder(pet.getType().getOeuf()).setName(pet.getName()).setLore("�aLevel : " + pet.getLevel()).toItemStack());
				count++;
			}
		} else {
			
		}
	}

}
