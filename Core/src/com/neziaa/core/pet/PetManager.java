package com.neziaa.core.pet;

import java.io.File;
import java.util.ArrayList;

import com.neziaa.core.Main;
import com.neziaa.core.utils.json.DiscUtil;
import com.neziaa.core.utils.json.JsonPersist;

import lombok.Getter;
import net.minecraft.util.com.google.common.reflect.TypeToken;

public class PetManager implements JsonPersist {
	
	@Getter private static PetManager instance;
	
	@Getter private ArrayList<Pet> pets = new ArrayList<>();

	@Override
	public File getFile() {
		return new File(Main.getInstance().getDataFolder(), "pets.json");
	}

	@Override
	public void loadData() {
		String content = DiscUtil.readCatch(getFile());
		if (content == null) return;
		
		@SuppressWarnings("serial")
		ArrayList<Pet> map =  JsonPersist.gson.fromJson(content, new TypeToken<ArrayList<Pet>>(){}.getType());
		
		pets.clear();
		pets.addAll(map);
	}

	@Override
	public void saveData(boolean sync) {
		DiscUtil.writeCatch(getFile(), JsonPersist.gson.toJson(pets), true);
	}
	
	public ArrayList<Pet> getPets(String owner){
		ArrayList<Pet> list = new ArrayList<Pet>();
		for(Pet pet: pets) {
			if(pet.getOwner().equalsIgnoreCase(owner)) {
				list.add(pet);
			}
		}
		return list;
	}

}
