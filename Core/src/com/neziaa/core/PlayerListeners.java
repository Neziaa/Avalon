package com.neziaa.core;

import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerListeners implements Listener {
	
	@EventHandler
	public void use(PlayerInteractEvent event) {
		if(event.getItem().getType().equals(Material.STICK)) {
			Player player = event.getPlayer();
			Location playerLoc = event.getPlayer().getLocation();
			Location cube1 = new Location(player.getWorld(), playerLoc.getX() - 1, playerLoc.getY() - 1, playerLoc.getZ() - 1);
			Location cube2 = new Location(player.getWorld(), playerLoc.getX() + 1, playerLoc.getY() + 1, playerLoc.getZ() + 1);
			
			Cuboid around = new Cuboid(cube1, cube2);
			int count = 0;
			for(Block block: around.getBlocks()) {
				if(block.getType().equals(Material.OBSIDIAN)) {
					block.setType(Material.COBBLESTONE);
					count++;
				}
			}
			
			event.getPlayer().sendMessage("�9" + count + " blocks changer en obsidienne moisie.");
			
			player.playEffect(EntityEffect.FIREWORK_EXPLODE);
		}
	}

}
