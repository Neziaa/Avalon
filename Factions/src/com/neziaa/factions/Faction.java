package com.neziaa.factions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.neziaa.factions.clan.Clan;
import com.neziaa.factions.iface.EconomyParticipator;
import com.neziaa.factions.iface.RelationParticipator;
import com.neziaa.factions.integration.Econ;
import com.neziaa.factions.struct.FFlag;
import com.neziaa.factions.struct.FPerm;
import com.neziaa.factions.struct.Rel;
import com.neziaa.factions.util.LazyLocation;
import com.neziaa.factions.util.MiscUtil;
import com.neziaa.factions.util.RelationUtil;
import com.neziaa.factions.zcore.persist.Entity;

import lombok.Getter;
import lombok.Setter;


public class Faction extends Entity implements EconomyParticipator {
	
	@Getter @Setter private String tag;
    @Getter @Setter private String description;
    @Getter @Setter private String cape;
    @Getter @Setter private Long lastOnlineTime; 
    @Getter @Setter private Double powerBoost;
    @Getter @Setter private boolean open;
    
    @Getter @Setter public  Double money;
    
    @Getter private Set<String> invites;
    
    private LazyLocation home;
    private Map<String, Rel> relationWish;
    private transient Set<FPlayer> fplayers = new HashSet<FPlayer>();
    
    private Map<FFlag, Boolean> flagOverrides;
    private Map<FPerm, Set<Rel>> permOverrides;
    
    /** Avalon **/
    
    @Getter @Setter private Clan clan;
    @Getter @Setter private ArrayList<Faction> allies = new ArrayList<Faction>();
    
    public Faction() {
        this.relationWish = new HashMap<String, Rel>();
        this.invites = new HashSet<String>();
        this.open = Conf.newFactionsDefaultOpen;
        this.tag = "???";
        this.description = "Description par d�faut...";
        this.money = 0.0;
        this.powerBoost = 0.0;
        this.flagOverrides = new LinkedHashMap<FFlag, Boolean>();
        this.permOverrides = new LinkedHashMap<FPerm, Set<Rel>>();
        
        this.clan = Clan.NONE;
     }

    public void invite(FPlayer fplayer) {
        this.invites.add(fplayer.getId().toLowerCase());
    }

    public void deinvite(FPlayer fplayer) {
        this.invites.remove(fplayer.getId().toLowerCase());
    }

    public boolean isInvited(FPlayer fplayer) {
        return this.invites.contains(fplayer.getId().toLowerCase());
    }

    public String getTag(String prefix) {
        return prefix + this.tag;
    }

    public String getTag(RelationParticipator observer) {
        if (observer == null) {
            return getTag();
        }
        return this.getTag(this.getColorTo(observer).toString());
    }

    public String getComparisonTag() {
        return MiscUtil.getComparisonString(this.tag);
    }
    
    
    public void setHome(Location home) {
        this.home = new LazyLocation(home);
    }

    public boolean hasHome() {
        return this.getHome() != null;
    }

    public Location getHome() {
        confirmValidHome();
        return (this.home != null) ? this.home.getLocation() : null;
    }

    public void confirmValidHome() {
        if (!Conf.homesMustBeInClaimedTerritory || this.home == null || (this.home.getLocation() != null && Boards.getFactionAt(new FLocation(this.home.getLocation())) == this))
            return;

        msg("�cVotre faction ne poss�de d�sormais plus de f home");
        this.home = null;
    }
    
    public String getAccountId() {
        String aid = "faction-" + this.getId();

        // We need to override the default money given to players.
        if (!Econ.hasAccount(aid))
            Econ.setBalance(aid, 0);

        return aid;
    }

    public boolean getFlag(FFlag flag) {
        Boolean ret = this.flagOverrides.get(flag);
        if (ret == null) ret = flag.getDefault();
        return ret;
    }

    public void setFlag(FFlag flag, boolean value) {
        if (Conf.factionFlagDefaults.get(flag).equals(value)) {
            this.flagOverrides.remove(flag);
            return;
        }
        this.flagOverrides.put(flag, value);
    }

    public Set<Rel> getPermittedRelations(FPerm perm) {
        Set<Rel> ret = this.permOverrides.get(perm);
        if (ret == null) ret = perm.getDefault();
        return ret;
    }

    public void setRelationPermitted(FPerm perm, Rel rel, boolean permitted) {
        Set<Rel> newPermittedRelations = EnumSet.noneOf(Rel.class);
        newPermittedRelations.addAll(this.getPermittedRelations(perm));
        if (permitted) {
            newPermittedRelations.add(rel);
        } else {
            newPermittedRelations.remove(rel);
        }
        this.setPermittedRelations(perm, newPermittedRelations);
    }

    public void setPermittedRelations(FPerm perm, Set<Rel> rels) {
        if (perm.getDefault().equals(rels)) {
            this.permOverrides.remove(perm);
            return;
        }
        this.permOverrides.put(perm, rels);
    }

    public void setPermittedRelations(FPerm perm, Rel... rels) {
        Set<Rel> temp = new HashSet<Rel>();
        temp.addAll(Arrays.asList(rels));
        this.setPermittedRelations(perm, temp);
    }


    // -------------------------------
    // Understand the types
    // -------------------------------

    public boolean isNormal() {
        return !this.isNone();
    }

    public boolean isNone() {
        return this.getId().equals("0");
    }
    
    public boolean getSafezone() {
    	return this.getId().equals("-1");
    }
    
    public boolean getWarzone() {
    	return this.getId().equals("-2");
    }
    
    public boolean getStaff() {
    	return this.getId().equals("-3");
    }
    
    public boolean getEvent() {
    	return this.getId().equals("-4");
    }

    // -------------------------------
    // Relation and relation colors
    // -------------------------------

    @Override
    public String describeTo(RelationParticipator observer, boolean ucfirst) {
        return RelationUtil.describeThatToMe(this, observer, ucfirst);
    }

    @Override
    public String describeTo(RelationParticipator observer) {
        return RelationUtil.describeThatToMe(this, observer);
    }

    @Override
    public Rel getRelationTo(RelationParticipator observer) {
        return RelationUtil.getRelationOfThatToMe(this, observer);
    }

    @Override
    public Rel getRelationTo(RelationParticipator observer, boolean ignorePeaceful) {
        return RelationUtil.getRelationOfThatToMe(this, observer, ignorePeaceful);
    }

    @Override
    public ChatColor getColorTo(RelationParticipator observer) {
        return RelationUtil.getColorOfThatToMe(this, observer);
    }

    public Rel getRelationWish(Faction otherFaction) {
        if (this.relationWish.containsKey(otherFaction.getId())) {
            return this.relationWish.get(otherFaction.getId());
        }
        return Rel.NEUTRAL;
    }

    public void setRelationWish(Faction otherFaction, Rel relation) {
        if (this.relationWish.containsKey(otherFaction.getId()) && relation.equals(Rel.NEUTRAL)) {
            this.relationWish.remove(otherFaction.getId());
        } else {
            this.relationWish.put(otherFaction.getId(), relation);
        }
    }

    public Map<Rel, List<String>> getFactionTagsPerRelation(RelationParticipator rp) {
        return getFactionTagsPerRelation(rp, false);
    }

    // onlyNonNeutral option provides substantial performance boost on large servers for listing only non-neutral factions
    public Map<Rel, List<String>> getFactionTagsPerRelation(RelationParticipator rp, boolean onlyNonNeutral) {
        Map<Rel, List<String>> ret = new HashMap<Rel, List<String>>();
        for (Rel rel : Rel.values()) {
            ret.put(rel, new ArrayList<String>());
        }
        for (Faction faction : Factions.i.get()) {
            Rel relation = faction.getRelationTo(this);
            if (onlyNonNeutral && relation == Rel.NEUTRAL) continue;
            ret.get(relation).add(faction.getTag(rp));
        }
        return ret;
    }

    // TODO: Implement a has enough feature.
    //----------------------------------------------//
    // Power
    //----------------------------------------------//
    public double getPower() {
        if (this.getFlag(FFlag.INFPOWER)) {
            return 999999;
        }

        double ret = 0;
        for (FPlayer fplayer : fplayers) {
            ret += fplayer.getPower();
        }
        if (Conf.powerFactionMax > 0 && ret > Conf.powerFactionMax) {
            ret = Conf.powerFactionMax;
        }
        return ret + this.powerBoost;
    }

    public double getPowerMax() {
        if (this.getFlag(FFlag.INFPOWER)) {
            return 999999;
        }

        double ret = 0;
        for (FPlayer fplayer : fplayers) {
            ret += fplayer.getPowerMax();
        }
        if (Conf.powerFactionMax > 0 && ret > Conf.powerFactionMax) {
            ret = Conf.powerFactionMax;
        }
        return ret + this.powerBoost;
    }

    public int getPowerRounded() {
        return (int) Math.round(this.getPower());
    }

    public int getPowerMaxRounded() {
        return (int) Math.round(this.getPowerMax());
    }

    public int getLandRounded() {
        return Boards.getFactionCoordCount(this);
    }

    public int getLandRoundedInWorld(String worldName) {
        return Boards.getFactionCoordCountInWorld(this, worldName);
    }

    public boolean hasLandInflation() {
        return this.getLandRounded() > this.getPowerRounded();
    }

    // -------------------------------
    // FPlayers
    // -------------------------------

    // maintain the reference list of FPlayers in this faction
    public void refreshFPlayers() {
        fplayers.clear();
        if (this.isNone()) return;

        for (FPlayer fplayer : FPlayers.i.get()) {
            if (fplayer.getFaction() == this) {
                fplayers.add(fplayer);
            }
        }
    }

    protected boolean addFPlayer(FPlayer fplayer) {
        if (this.isNone()) return false;

        return fplayers.add(fplayer);
    }

    protected boolean removeFPlayer(FPlayer fplayer) {
        if (this.isNone()) return false;

        return fplayers.remove(fplayer);
    }

    public Set<FPlayer> getFPlayers() {
        // return a shallow copy of the FPlayer list, to prevent tampering and concurrency issues
        Set<FPlayer> ret = new HashSet<FPlayer>(fplayers);
        return ret;
    }

    public Set<FPlayer> getFPlayersWhereOnline(boolean online) {
        Set<FPlayer> ret = new HashSet<FPlayer>();

        for (FPlayer fplayer : fplayers) {
            if (fplayer.isOnline() == online) {
                ret.add(fplayer);
            }
        }

        return ret;
    }

    public FPlayer getFPlayerLeader() {
        for (FPlayer fplayer : fplayers) {
            if (fplayer.getRole() == Rel.LEADER) {
                return fplayer;
            }
        }
        return null;
    }

    public ArrayList<FPlayer> getFPlayersWhereRole(Rel role) {
        ArrayList<FPlayer> ret = new ArrayList<FPlayer>();
        //if ( ! this.isNormal()) return ret;

        for (FPlayer fplayer : fplayers) {
            if (fplayer.getRole() == role) {
                ret.add(fplayer);
            }
        }

        return ret;
    }

    @SuppressWarnings("deprecation")
	public ArrayList<Player> getOnlinePlayers() {
        ArrayList<Player> ret = new ArrayList<Player>();
        //if (this.isPlayerFreeType()) return ret;

        for (Player player : P.p.getServer().getOnlinePlayers()) {
            FPlayer fplayer = FPlayers.i.get(player);
            if (fplayer.getFaction() == this) {
                ret.add(player);
            }
        }

        return ret;
    }

    // used when current leader is about to be removed from the faction; promotes new leader, or disbands faction if no other members left
    public void promoteNewLeader() {
        if (!this.isNormal()) return;
        if (this.getFlag(FFlag.PERMANENT) && Conf.permanentFactionsDisableLeaderPromotion) return;

        FPlayer oldLeader = this.getFPlayerLeader();

        // get list of officers, or list of normal members if there are no officers
        ArrayList<FPlayer> replacements = this.getFPlayersWhereRole(Rel.OFFICER);
        if (replacements == null || replacements.isEmpty())
            replacements = this.getFPlayersWhereRole(Rel.MEMBER);

        if (replacements == null || replacements.isEmpty()) {    // faction leader is the only member; one-man faction
            if (this.getFlag(FFlag.PERMANENT)) {
                if (oldLeader != null)
                    oldLeader.setRole(Rel.MEMBER);
                return;
            }

            // no members left and faction isn't permanent, so disband it
            if (Conf.logFactionDisband)
                P.p.log("The faction " + this.getTag() + " (" + this.getId() + ") has been disbanded since it has no members left.");

            for (FPlayer fplayer : FPlayers.i.getOnline()) {
                fplayer.msg("The faction %s<i> was disbanded.", this.getTag(fplayer));
            }

            this.detach();
        } else {    // promote new faction leader
            if (oldLeader != null)
                oldLeader.setRole(Rel.MEMBER);
            replacements.get(0).setRole(Rel.LEADER);
            this.msg("<i>Faction leader <h>%s<i> has been removed. %s<i> has been promoted as the new faction leader.", oldLeader == null ? "" : oldLeader.getName(), replacements.get(0).getName());
            P.p.log("Faction " + this.getTag() + " (" + this.getId() + ") leader was removed. Replacement leader: " + replacements.get(0).getName());
        }
    }

    //----------------------------------------------//
    // Messages
    //----------------------------------------------//
    public void msg(String message, Object... args) {
        message = P.p.txt.parse(message, args);

        for (FPlayer fplayer : this.getFPlayersWhereOnline(true)) {
            fplayer.sendMessage(message);
        }
    }

    public void sendMessage(String message) {
        for (FPlayer fplayer : this.getFPlayersWhereOnline(true)) {
            fplayer.sendMessage(message);
        }
    }

    public void sendMessage(List<String> messages) {
        for (FPlayer fplayer : this.getFPlayersWhereOnline(true)) {
            fplayer.sendMessage(messages);
        }
    }

    //----------------------------------------------//
    // Offline Faction Protection
    //----------------------------------------------//
    public void updateLastOnlineTime() {
        // We have either gained or a lost a player.
        if (this.isNone())
            return;

        this.lastOnlineTime = System.currentTimeMillis();
    }

    public boolean hasOfflineExplosionProtection() {
        if (!Conf.protectOfflineFactionsFromExplosions || this.isNone())
            return false;

        long timeUntilNoboom = this.getLastOnlineTime() + (long) (Conf.offlineExplosionProtectionDelay * 60 * 1000);

        //No protection if players are online.
        if (this.getOnlinePlayers().size() > 0)
            return false;

        // No Protection while timeUntilNoboom is greater than current system time.
        if (timeUntilNoboom > System.currentTimeMillis())
            return false;

        return true;
    }

    //----------------------------------------------//
    // Deprecated
    //----------------------------------------------//

    public FPlayer getFPlayerAdmin() {
        return getFPlayerLeader();
    }

    public boolean isPeaceful() {
        return this.getFlag(FFlag.PEACEFUL);
    }

    public boolean getPeacefulExplosionsEnabled() {
        return this.getFlag(FFlag.EXPLOSIONS);
    }

    public boolean noExplosionsInTerritory() {
        return !this.getFlag(FFlag.EXPLOSIONS);
    }

    public boolean isSafeZone() {
        return !this.getFlag(FFlag.EXPLOSIONS);
    }

    //----------------------------------------------//
    // Persistance and entity management
    //----------------------------------------------//


    @Override
    public void postDetach() {
        if (Econ.shouldBeUsed()) {
            Econ.setBalance(getAccountId(), 0);
        }

        // Clean the board
        Boards.clean();

        // Clean the fplayers
        FPlayers.i.clean();
    }
}
