package com.neziaa.factions;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.neziaa.factions.event.FPlayerLeaveEvent;
import com.neziaa.factions.event.LandClaimEvent;
import com.neziaa.factions.iface.EconomyParticipator;
import com.neziaa.factions.iface.RelationParticipator;
import com.neziaa.factions.integration.Econ;
import com.neziaa.factions.integration.Worldguard;
import com.neziaa.factions.scoreboards.FScoreboard;
import com.neziaa.factions.scoreboards.sidebar.FDefaultSidebar;
import com.neziaa.factions.scoreboards.sidebar.FInfoSidebar;
import com.neziaa.factions.struct.FFlag;
import com.neziaa.factions.struct.FPerm;
import com.neziaa.factions.struct.Rel;
import com.neziaa.factions.util.RelationUtil;
import com.neziaa.factions.zcore.persist.PlayerEntity;

import lombok.Getter;
import lombok.Setter;


/**
 * Logged in players always have exactly one FPlayer instance.
 * Logged out players may or may not have an FPlayer instance. They will always have one if they are part of a faction.
 * This is because only players with a faction are saved to disk (in order to not waste disk space).
 * <p/>
 * The FPlayer is linked to a minecraft player using the player name.
 * <p/>
 * The same instance is always returned for the same player.
 * This means you can use the == operator. No .equals method necessary.
 */

// TODO: The players are saved in non order.
public class FPlayer extends PlayerEntity implements EconomyParticipator {
	
	@Getter @Setter private transient FLocation lastStoodAt = new FLocation();
    @Getter @Setter private transient boolean mapAutoUpdating;
    @Getter @Setter private transient Faction autoClaimFor;
    
    @Getter @Setter private Rel role;
    @Getter @Setter private double powerBoost;
    @Getter @Setter private String title;
    
    @Setter private transient boolean hasAdminMode = false;
	
    private String factionId;
    
    private double power;

    private long lastPowerUpdateTime;

    private long lastLoginTime;
    
    private transient boolean loginPvpDisabled;

    public Faction getFaction() {
        if (this.factionId == null) {
            return null;
        }
        return Factions.i.get(this.factionId);
    }

    public String getFactionId() {
        return this.factionId;
    }

    public boolean hasFaction() {
        return !factionId.equals("0");
    }

    public void setFaction(Faction faction) {
        Faction oldFaction = this.getFaction();
        if (oldFaction != null) oldFaction.removeFPlayer(this);
        faction.addFPlayer(this);
        this.factionId = faction.getId();
    }

    // FIELD: account
    public String getAccountId() {
        return this.getId();
    }

    // -------------------------------------------- //
    // Construct
    // -------------------------------------------- //

    // GSON need this noarg constructor.
    public FPlayer() {
        this.resetFactionData(false);
        this.power = Conf.powerPlayerStarting;
        this.lastPowerUpdateTime = System.currentTimeMillis();
        this.lastLoginTime = System.currentTimeMillis();
        this.mapAutoUpdating = false;
        this.autoClaimFor = null;
        this.loginPvpDisabled = (Conf.noPVPDamageToOthersForXSecondsAfterLogin > 0) ? true : false;
        this.powerBoost = 0.0;

        if (!Conf.newPlayerStartingFactionID.equals("0") && Factions.i.exists(Conf.newPlayerStartingFactionID)) {
            this.factionId = Conf.newPlayerStartingFactionID;
        }
    }

    public final void resetFactionData(boolean doSpoutUpdate) {
        if (this.factionId != null && Factions.i.exists(this.factionId)) // Avoid infinite loop! TODO: I think that this is needed is a sign we need to refactor.
        {
            Faction currentFaction = this.getFaction();
            if (currentFaction != null) {
                currentFaction.removeFPlayer(this);
            }
        }

        this.factionId = "0"; // The default neutral faction

        this.role = Rel.MEMBER;
        this.title = "";
        this.autoClaimFor = null;
    }

    public void resetFactionData() {
        this.resetFactionData(true);
    }

    // -------------------------------------------- //
    // Getters And Setters
    // -------------------------------------------- //
    
    public boolean hasAdminMode() {
    	return hasAdminMode;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        losePowerFromBeingOffline();
        this.lastLoginTime = lastLoginTime;
        this.lastPowerUpdateTime = lastLoginTime;
        if (Conf.noPVPDamageToOthersForXSecondsAfterLogin > 0) {
            this.loginPvpDisabled = true;
        }
    }

    public boolean hasLoginPvpDisabled() {
        if (!loginPvpDisabled) {
            return false;
        }
        if (this.lastLoginTime + (Conf.noPVPDamageToOthersForXSecondsAfterLogin * 1000) < System.currentTimeMillis()) {
            this.loginPvpDisabled = false;
            return false;
        }
        return true;
    }

    //----------------------------------------------//
    // Title, Name, Faction Tag and Chat
    //----------------------------------------------//
    public String getName() {
        if (isOnline()) {
            return getPlayer().getName();
        }
        OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(getId()));
        return player.getName() != null ? player.getName() : getId();
    }

    public String getTag() {
        return this.hasFaction() ? this.getFaction().getTag() : "";
    }

    // Base concatenations:

    public String getNameAndSomething(String something) {
        String ret = this.role.getPrefix();
        if (something.length() > 0) {
            ret += something + " ";
        }
        ret += this.getName();
        return ret;
    }

    public String getNameAndTitle() {
        return this.getNameAndSomething(this.getTitle());
    }

    public String getNameAndTag() {
        return this.getNameAndSomething(this.getTag());
    }

    // Colored concatenations:
    // These are used in information messages

    public String getNameAndTitle(Faction faction) {
        return this.getColorTo(faction) + this.getNameAndTitle();
    }

    public String getNameAndTitle(FPlayer fplayer) {
        return this.getColorTo(fplayer) + this.getNameAndTitle();
    }

    // Chat Tag:
    // These are injected into the format of global chat messages.

    public String getChatTag() {
        if (!this.hasFaction()) {
            return "";
        }

        return String.format(Conf.chatTagFormat, this.role.getPrefix() + this.getTag());
    }

    // Colored Chat Tag
    public String getChatTag(Faction faction) {
        if (!this.hasFaction()) {
            return "";
        }

        return this.getRelationTo(faction).getColor() + getChatTag();
    }

    public String getChatTag(FPlayer fplayer) {
        if (!this.hasFaction()) {
            return "";
        }

        return this.getColorTo(fplayer) + getChatTag();
    }

    // -------------------------------
    // Relation and relation colors
    // -------------------------------

    @Override
    public String describeTo(RelationParticipator observer, boolean ucfirst) {
        return RelationUtil.describeThatToMe(this, observer, ucfirst);
    }

    @Override
    public String describeTo(RelationParticipator observer) {
        return RelationUtil.describeThatToMe(this, observer);
    }

    @Override
    public Rel getRelationTo(RelationParticipator observer) {
        return RelationUtil.getRelationOfThatToMe(this, observer);
    }

    @Override
    public Rel getRelationTo(RelationParticipator observer, boolean ignorePeaceful) {
        return RelationUtil.getRelationOfThatToMe(this, observer, ignorePeaceful);
    }

    public Rel getRelationToLocation() {
        return Boards.getFactionAt(new FLocation(this)).getRelationTo(this);
    }

    @Override
    public ChatColor getColorTo(RelationParticipator observer) {
        return RelationUtil.getColorOfThatToMe(this, observer);
    }

    //----------------------------------------------//
    // Health
    //----------------------------------------------//

    // Delete ... Bug with getHealth (ambigous) 

    //----------------------------------------------//
    // Power
    //----------------------------------------------//
    public double getPower() {
        this.updatePower();
        return this.power;
    }

    protected void alterPower(double delta) {
        this.power += delta;
        if (this.power > this.getPowerMax())
            this.power = this.getPowerMax();
        else if (this.power < this.getPowerMin())
            this.power = this.getPowerMin();
    }

    public double getPowerMax() {
        return Conf.powerPlayerMax + this.powerBoost;
    }

    public double getPowerMin() {
        return Conf.powerPlayerMin + this.powerBoost;
    }

    public int getPowerRounded() {
        return (int) Math.round(this.getPower());
    }

    public int getPowerMaxRounded() {
        return (int) Math.round(this.getPowerMax());
    }

    public int getPowerMinRounded() {
        return (int) Math.round(this.getPowerMin());
    }

    protected void updatePower() {
        if (this.isOffline()) {
            losePowerFromBeingOffline();
            if (!Conf.powerRegenOffline) {
                return;
            }
        }
        long now = System.currentTimeMillis();
        long millisPassed = now - this.lastPowerUpdateTime;
        this.lastPowerUpdateTime = now;

        Player thisPlayer = this.getPlayer();
        if (thisPlayer != null && thisPlayer.isDead())
            return;  // don't let dead players regain power until they respawn

        int millisPerMinute = 60 * 1000;
        double powerPerMinute = Conf.powerPerMinute;
        if (Conf.scaleNegativePower && this.power < 0) {
            powerPerMinute += (Math.sqrt(Math.abs(this.power)) * Math.abs(this.power)) / Conf.scaleNegativeDivisor;
        }
        this.alterPower(millisPassed * powerPerMinute / millisPerMinute);

    }

    protected void losePowerFromBeingOffline() {
        if (Conf.powerOfflineLossPerDay > 0.0 && this.power > Conf.powerOfflineLossLimit) {
            long now = System.currentTimeMillis();
            long millisPassed = now - this.lastPowerUpdateTime;
            this.lastPowerUpdateTime = now;

            double loss = millisPassed * Conf.powerOfflineLossPerDay / (24 * 60 * 60 * 1000);
            if (this.power - loss < Conf.powerOfflineLossLimit) {
                loss = this.power;
            }
            this.alterPower(-loss);
        }
    }

    public void onDeath() {
        this.updatePower();
        this.alterPower(-Conf.powerPerDeath);
    }

    //----------------------------------------------//
    // Territory
    //----------------------------------------------//
    public boolean isInOwnTerritory() {
        return Boards.getFactionAt(new FLocation(this)) == this.getFaction();
    }

	public boolean isInAllyTerritory(){
		return Boards.getFactionAt(new FLocation(this)).getRelationTo(this) == Rel.ALLY;
	}


    public boolean isInEnemyTerritory() {
        return Boards.getFactionAt(new FLocation(this)).getRelationTo(this) == Rel.ENEMY;
    }

    public void sendFactionHereMessage() {
        Faction toShow = Boards.getFactionAt(getLastStoodAt());
        if (shouldShowScoreboard(toShow)) {
            // Setup the scoreboard if it is not already.
            if (FScoreboard.get(this) == null) {
                // Initialize the scoreboard
                FScoreboard.init(this);
                if (P.p.getConfig().getBoolean("scoreboard.default-enabled", false)) {
                    FScoreboard.get(this).setDefaultSidebar(new FDefaultSidebar(), P.p.getConfig().getInt("default-update-interval", 20));
                }
                FScoreboard.get(this).setSidebarVisibility(P.p.cmdBase.cmdScoreBoard.showBoard(this));
            }

            // Shows them the scoreboard instead of sending a message in chat. Will disappear after a few seconds.
            FScoreboard.get(this).setTemporarySidebar(new FInfoSidebar(toShow));
            // Shows the faction description via message
            if (P.p.getConfig().getBoolean("scoreboard.default-enabled", false) && !toShow.isNone()) {
                if (toShow.getDescription().length() > 0) {
                    String msg = ChatColor.AQUA + "" + ChatColor.BOLD + "> " + ChatColor.RESET + P.p.txt.parse("<i>") + " ~ " + toShow.getTag(this);
                    msg += toShow.getDescription();
                    this.sendMessage(msg);
                }
            }
        } else {
            String msg = P.p.txt.parse("<i>") + " ~ " + toShow.getTag(this);
            if (toShow.getDescription().length() > 0) {
                msg += " - " + toShow.getDescription();
            }
            this.sendMessage(msg);
        }
    }

    /**
     * Check if the scoreboard should be shown. Simple method to be used by above method.
     *
     * @param toShow Faction to be shown.
     * @return true if should show, otherwise false.
     */
    
    @SuppressWarnings("unlikely-arg-type")
	private boolean shouldShowScoreboard(Faction toShow) {
        return !toShow.getId().equals(-2) && !toShow.isNone() && !toShow.getId().equals(-1) && P.p.getConfig().contains("scoreboard.finfo") && P.p.getConfig().getBoolean("scoreboard.finfo-enabled", false) && P.p.cmdBase.cmdScoreBoard.showBoard(this);
    }

    // -------------------------------
    // Actions
    // -------------------------------

    public void leave(boolean makePay) {
        Faction myFaction = this.getFaction();
        makePay = makePay && Econ.shouldBeUsed() && !this.hasAdminMode();

        if (myFaction == null) {
            resetFactionData();
            return;
        }

        boolean perm = myFaction.getFlag(FFlag.PERMANENT);

        if (!perm && this.getRole() == Rel.LEADER && myFaction.getFPlayers().size() > 1) {
            msg("<b>Vous devez donner le role de <h>leader <b> � quelqu'un d'autre que vous avant de quittez!");
            return;
        }

        if (!Conf.canLeaveWithNegativePower && this.getPower() < 0) {
            msg("<b>You cannot leave until your power is positive.");
            return;
        }

        // if economy is enabled and they're not on the bypass list, make sure they can pay
        if (makePay && !Econ.hasAtLeast(this, Conf.econCostLeave, "to leave your faction.")) return;

        FPlayerLeaveEvent leaveEvent = new FPlayerLeaveEvent(this, myFaction, FPlayerLeaveEvent.PlayerLeaveReason.LEAVE);
        Bukkit.getServer().getPluginManager().callEvent(leaveEvent);
        if (leaveEvent.isCancelled()) return;

        // then make 'em pay (if applicable)
        if (makePay && !Econ.modifyMoney(this, -Conf.econCostLeave, "to leave your faction.", "for leaving your faction."))
            return;

        // Am I the last one in the faction?
        if (myFaction.getFPlayers().size() == 1) {
            // Transfer all money
            if (Econ.shouldBeUsed())
                Econ.transferMoney(this, myFaction, this, Econ.getBalance(myFaction.getAccountId()));
        }

        if (myFaction.isNormal()) {
            for (FPlayer fplayer : myFaction.getFPlayersWhereOnline(true)) {
            	fplayer.msg("<h>%s <i> vient de <b>quitter <i>la faction!", this.getNameAndTitle());
            }

            if (Conf.logFactionLeave)
                P.p.log(this.getName() + " left the faction: " + myFaction.getTag());
        }

        this.resetFactionData();
        myFaction.updateLastOnlineTime();

        if (myFaction.isNormal() && !perm && myFaction.getFPlayers().isEmpty()) {
            // Remove this faction
            for (FPlayer fplayer : FPlayers.i.getOnline()) {
                fplayer.msg("<i>%s<i> vient d'�tre disband.", myFaction.describeTo(fplayer, true));
            }

            myFaction.detach();
            if (Conf.logFactionDisband)
                P.p.log("The faction " + myFaction.getTag() + " (" + myFaction.getId() + ") was disbanded due to the last player (" + this.getName() + ") leaving.");
        }
    }

    public boolean canClaimForFactionAtLocation(Faction forFaction, Location location, boolean notifyFailure) {
        String error = null;
        FLocation flocation = new FLocation(location);
        Faction myFaction = getFaction();
        Faction currentFaction = Boards.getFactionAt(flocation);
        int ownedLand = forFaction.getLandRounded();

        if (Conf.worldGuardChecking && Worldguard.checkForRegionsInChunk(location)) {
            // Checks for WorldGuard regions in the chunk attempting to be claimed
            error = P.p.txt.parse("<b>Ce territoire est proteger.");
        } else if (Conf.worldsNoClaiming.contains(flocation.getWorldName())) {
            error = P.p.txt.parse("<b>Ce territoire est proteger.");
        } else if (this.hasAdminMode()) {
            return true;
        } else if (forFaction == currentFaction) {
            error = P.p.txt.parse("%s<i> possede d�j� ce territoire.", forFaction.describeTo(this, true));
        } else if (!FPerm.TERRITORY.has(this, forFaction, true)) {
            return false;
        } else if (forFaction.getFPlayers().size() < Conf.claimsRequireMinFactionMembers) {
            error = P.p.txt.parse("Factions must have at least <h>%s<b> members to claim land.", Conf.claimsRequireMinFactionMembers);
        } else if (ownedLand >= forFaction.getPowerRounded()) {
            error = P.p.txt.parse("<b>Vous ne pouvez pas claim ce territoire, il vous faut plus de power!");
        } else if (Conf.claimedLandsMax != 0 && ownedLand >= Conf.claimedLandsMax && !forFaction.getFlag(FFlag.INFPOWER)) {
            error = P.p.txt.parse("<b>Vous ne pouvez pas claim ce territoire.");
        } else if (!Conf.claimingFromOthersAllowed && currentFaction.isNormal()) {
            error = P.p.txt.parse("<b>You may not claim land from others.");
        } else if (currentFaction.getRelationTo(forFaction).isAtLeast(Rel.ALLY) && !currentFaction.isNone()) {
            error = P.p.txt.parse("<b>Vous ne pouvez pas claim des territoire alli�s!");
        } else if
                (
                Conf.claimsMustBeConnected
                        && !this.hasAdminMode()
                        && myFaction.getLandRoundedInWorld(flocation.getWorldName()) > 0
                        && !Boards.isConnectedLocation(flocation, myFaction)
                        && (!Conf.claimsCanBeUnconnectedIfOwnedByOtherFaction || !currentFaction.isNormal())
                ) {
            if (Conf.claimsCanBeUnconnectedIfOwnedByOtherFaction)
                error = P.p.txt.parse("<b>You can only claim additional land which is connected to your first claim or controlled by another faction!");
            else
                error = P.p.txt.parse("<b>You can only claim additional land which is connected to your first claim!");
        } else if (currentFaction.isNormal()) {
            if (!currentFaction.hasLandInflation()) {
                // TODO more messages WARN current faction most importantly
                error = P.p.txt.parse("%s<i> owns this land and is strong enough to keep it.", currentFaction.getTag(this));
            } else if (!Boards.isBorderLocation(flocation)) {
                error = P.p.txt.parse("<b>You must start claiming land at the border of the territory.");
            }
        }

        if (notifyFailure && error != null) {
            msg(error);
        }
        return error == null;
    }

    public boolean attemptClaim(Faction forFaction, Location location, boolean notifyFailure) {
        // notifyFailure is false if called by auto-claim; no need to notify on every failure for it
        // return value is false on failure, true on success

        FLocation flocation = new FLocation(location);
        Faction currentFaction = Boards.getFactionAt(flocation);

        int ownedLand = forFaction.getLandRounded();

        if (!this.canClaimForFactionAtLocation(forFaction, location, notifyFailure)) return false;

        // TODO: Add flag no costs??
        // if economy is enabled and they're not on the bypass list, make sure they can pay
        boolean mustPay = Econ.shouldBeUsed() && !this.hasAdminMode();
        double cost = 0.0;
        EconomyParticipator payee = null;
        if (mustPay) {
            cost = Econ.calculateClaimCost(ownedLand, currentFaction.isNormal());

            if (Conf.econClaimUnconnectedFee != 0.0 && forFaction.getLandRoundedInWorld(flocation.getWorldName()) > 0 && !Boards.isConnectedLocation(flocation, forFaction))
                cost += Conf.econClaimUnconnectedFee;

            if (Conf.bankEnabled && Conf.bankFactionPaysLandCosts && this.hasFaction())
                payee = this.getFaction();
            else
                payee = this;

            if (!Econ.hasAtLeast(payee, cost, "to claim this land")) return false;
        }

        LandClaimEvent claimEvent = new LandClaimEvent(flocation, forFaction, this);
        Bukkit.getServer().getPluginManager().callEvent(claimEvent);
        if (claimEvent.isCancelled()) {
        	return false;
        }

        // announce success
        Set<FPlayer> informTheseFPlayers = new HashSet<FPlayer>();
        informTheseFPlayers.add(this);
        informTheseFPlayers.addAll(forFaction.getFPlayersWhereOnline(true));
        for (FPlayer fp : informTheseFPlayers) {
            fp.msg("<h>%s<i> a claim pour <h>%s<i> sur le claim de la faction <h>%s<i>.", this.describeTo(fp, true), forFaction.describeTo(fp), currentFaction.describeTo(fp));
        }

        Boards.setFactionAt(forFaction, flocation);

        if (Conf.logLandClaims) {
            P.p.log(this.getName() + " vient de claim (" + flocation.getCoordString() + ") pour la faction: " + forFaction.getTag());
        }

        return true;
    }

    // -------------------------------------------- //
    // Persistance
    // -------------------------------------------- //

    @Override
    public boolean shouldBeSaved() {
        if (this.hasFaction()) return true;
        if (this.getPowerRounded() != this.getPowerMaxRounded() && this.getPowerRounded() != (int) Math.round(Conf.powerPlayerStarting))
            return true;
        return false;
    }

    public void msg(String str, Object... args) {
        this.sendMessage(P.p.txt.parse(str, args));
    }
}
