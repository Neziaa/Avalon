package com.neziaa.factions.util;

import com.neziaa.factions.Conf;
import com.neziaa.factions.Factions;
import com.neziaa.factions.P;

public class EconLandRewardTask implements Runnable {

    double rate;

    public EconLandRewardTask() {
        this.rate = Conf.econLandRewardTaskRunsEveryXMinutes;
    }

    @Override
    public void run() {
        Factions.i.econLandRewardRoutine();
        // maybe setting has been changed? if so, restart task at new rate
        if (this.rate != Conf.econLandRewardTaskRunsEveryXMinutes)
            P.p.startEconLandRewardTask(true);
    }

}
