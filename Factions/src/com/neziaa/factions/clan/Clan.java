package com.neziaa.factions.clan;

import lombok.Getter;

@Getter
public enum Clan {
	
	NONE("Aucun", ""), 
	
	VALLOST("Vallost", ""),
	
	GHAEILL("Ghaeill", "");
	
	private String name;
	private String description;
	
	private Clan(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public static Clan getClan(String name) {
		for(Clan clan: values()) {
			if(clan.getName().equalsIgnoreCase(name)) {
				return clan;
			}
		}
		return NONE;
	}
}
