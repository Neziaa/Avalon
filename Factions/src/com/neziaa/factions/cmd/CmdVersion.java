package com.neziaa.factions.cmd;

import com.neziaa.factions.struct.Permission;


public class CmdVersion extends FCommand {
    public CmdVersion() {
        this.aliases.add("version");

        //this.requiredArgs.add("");
        //this.optionalArgs.put("", "");

        this.permission = Permission.VERSION.node;
        this.disableOnLock = false;

        senderMustBePlayer = false;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        msg("<g>Factions UUID 2.0 - Avalon");
    }
}
