package com.neziaa.factions.cmd;

import com.neziaa.factions.struct.Rel;

public class CmdRelationTruce extends FRelationCommand {
    public CmdRelationTruce() {
        aliases.add("truce");
        targetRelation = Rel.TRUCE;
    }
}
