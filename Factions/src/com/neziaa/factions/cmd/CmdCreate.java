package com.neziaa.factions.cmd;

import com.neziaa.factions.*;
import com.neziaa.factions.event.FPlayerJoinEvent;
import com.neziaa.factions.event.FactionCreateEvent;
import com.neziaa.factions.struct.Permission;
import com.neziaa.factions.struct.Rel;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;

public class CmdCreate extends FCommand {
    public CmdCreate() {
        super();
        this.aliases.add("create");

        this.requiredArgs.add("faction tag");

        this.permission = Permission.CREATE.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        String tag = this.argAsString(0);

        if (fme.hasFaction()) {
            msg("<b>Vous avez d�j� une faction.");
            return;
        }

        if (Factions.i.isTagTaken(tag)) {
            msg("<b>Ce nom est d�j� pris.");
            return;
        }

        ArrayList<String> tagValidationErrors = Factions.validateTag(tag);
        if (tagValidationErrors.size() > 0) {
            sendMessage(tagValidationErrors);
            return;
        }

        // trigger the faction creation event (cancellable)
        FactionCreateEvent createEvent = new FactionCreateEvent(me, tag);
        Bukkit.getServer().getPluginManager().callEvent(createEvent);
        if (createEvent.isCancelled()) return;
        
        Faction faction = Factions.i.create();

        if (faction == null) {
            msg("<b>Error.");
            return;
        }

        // finish setting up the Faction
        faction.setTag(tag);

        // trigger the faction join event for the creator
        FPlayerJoinEvent joinEvent = new FPlayerJoinEvent(FPlayers.i.get(me), faction, FPlayerJoinEvent.PlayerJoinReason.CREATE);
        Bukkit.getServer().getPluginManager().callEvent(joinEvent);
        // join event cannot be cancelled or you'll have an empty faction

        // finish setting up the FPlayer
        fme.setRole(Rel.LEADER);
        fme.setFaction(faction);
        
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&8[&9Factions&8] &9" + fme.getName() + " &7vient de cr�er la faction &c" + faction.getTag() + " &7!"));
        fme.sendMessage("<i>Vous venez de cr�er la faction " + faction.getTag());
        
        msg("<i>Tape /f classement pour voir ta position au classement.");

        if (Conf.logFactionCreate)
            P.p.log(fme.getName() + " vient de cr�er la faction " + tag);
    }

}
