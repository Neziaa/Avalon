package com.neziaa.factions.cmd;

import com.neziaa.factions.Conf;
import com.neziaa.factions.P;
import com.neziaa.factions.iface.EconomyParticipator;
import com.neziaa.factions.integration.Econ;
import com.neziaa.factions.struct.Permission;

import org.bukkit.ChatColor;


public class CmdMoneyTransferPf extends FCommand {
    public CmdMoneyTransferPf() {
        this.aliases.add("pf");

        this.requiredArgs.add("amount");
        this.requiredArgs.add("player");
        this.requiredArgs.add("faction");

        //this.optionalArgs.put("", "");

        this.permission = Permission.MONEY_P2F.node;
        this.setHelpShort("transfer p -> f");

        senderMustBePlayer = false;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        double amount = this.argAsDouble(0, 0d);
        EconomyParticipator from = this.argAsBestFPlayerMatch(1);
        if (from == null) return;
        EconomyParticipator to = this.argAsFaction(2);
        if (to == null) return;

        boolean success = Econ.transferMoney(fme, from, to, amount);

        if (success && Conf.logMoneyTransactions)
            P.p.log(ChatColor.stripColor(P.p.txt.parse("%s transferred %s from the player \"%s\" to the faction \"%s\"", fme.getName(), Econ.moneyString(amount), from.describeTo(null), to.describeTo(null))));
    }
}
