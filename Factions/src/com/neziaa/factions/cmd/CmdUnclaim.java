package com.neziaa.factions.cmd;

import com.neziaa.factions.*;
import com.neziaa.factions.event.LandUnclaimEvent;
import com.neziaa.factions.integration.Econ;
import com.neziaa.factions.struct.FPerm;
import com.neziaa.factions.struct.Permission;

import org.bukkit.Bukkit;

public class CmdUnclaim extends FCommand {
    public CmdUnclaim() {
        this.aliases.add("unclaim");
        this.aliases.add("declaim");

        //this.requiredArgs.add("");
        //this.optionalArgs.put("", "");

        this.permission = Permission.UNCLAIM.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        FLocation flocation = new FLocation(fme);
        Faction otherFaction = Boards.getFactionAt(flocation);

        if (!FPerm.TERRITORY.has(sender, otherFaction, true)) return;

        LandUnclaimEvent unclaimEvent = new LandUnclaimEvent(flocation, otherFaction, fme);
        Bukkit.getServer().getPluginManager().callEvent(unclaimEvent);
        if (unclaimEvent.isCancelled()) return;

        //String moneyBack = "<i>";
        if (Econ.shouldBeUsed()) {
            double refund = Econ.calculateClaimRefund(myFaction.getLandRounded());

            if (Conf.bankEnabled && Conf.bankFactionPaysLandCosts) {
                if (!Econ.modifyMoney(myFaction, refund, "to unclaim this land", "for unclaiming this land")) return;
            } else {
                if (!Econ.modifyMoney(fme, refund, "to unclaim this land", "for unclaiming this land")) return;
            }
        }

        Boards.removeAt(flocation);
        myFaction.msg("%s<i> unclaimed some land.", fme.describeTo(myFaction, true));

        if (Conf.logLandUnclaims)
            P.p.log(fme.getName() + " unclaimed land at (" + flocation.getCoordString() + ") from the faction: " + otherFaction.getTag());
    }

}
