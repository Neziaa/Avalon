package com.neziaa.factions.cmd;

import com.neziaa.factions.Boards;
import com.neziaa.factions.FLocation;
import com.neziaa.factions.FPlayer;
import com.neziaa.factions.Faction;
import com.neziaa.factions.struct.FPerm;
import com.neziaa.factions.struct.Permission;
import com.neziaa.factions.struct.TerritoryAccess;
import com.neziaa.factions.zcore.util.TextUtil;


public class CmdAccess extends FCommand {
    public CmdAccess() {
        super();
        this.aliases.add("access");

        this.optionalArgs.put("view|p|f|player|faction", "view");
        this.optionalArgs.put("name", "you");

        this.setHelpShort("view or grant access for the claimed territory you are in");

        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        String type = this.argAsString(0);
        type = (type == null) ? "" : type.toLowerCase();
        FLocation loc = new FLocation(me.getLocation());

        TerritoryAccess territory = Boards.getTerritoryAccessAt(loc);
        Faction locFaction = territory.getHostFaction();
        boolean accessAny = Permission.ACCESS_ANY.has(sender, false);

        if (type.isEmpty() || type.equals("view")) {
            if (!accessAny && !Permission.ACCESS_VIEW.has(sender, true)) return;
            if (!accessAny && !territory.doesHostFactionMatch(fme)) {
                msg("<b>Le Territoire n'appartient pas � votre faction, donc vous ne pouvez pas voir la liste d'acc�s.");
                return;
            }
            showAccessList(territory, locFaction);
            return;
        }

        if (!accessAny && !Permission.ACCESS.has(sender, true)) return;
        if (!accessAny && !FPerm.ACCESS.has(fme, locFaction, true)) return;

        boolean doPlayer = true;
        if (type.equals("f") || type.equals("faction")) {
            doPlayer = false;
        } else if (!type.equals("p") && !type.equals("player")) {
            msg("<b>Vous �tes oblig� de sp�cifi� \"p\" ou \"player\" pour indiquer un joueur ou \"f\" ou \"faction\" pour indiquer une faction.");
            msg("<b>Exemple: /f access p IceExpert  -ou-  /f access f Avalon");
            msg("<b>Vous pouvez aussi utiliser cette commande sans arguments ou avec \"view\" pour voir les personnes acc�s.");
            return;
        }

        String target = "";
        boolean added;

        if (doPlayer) {
            FPlayer targetPlayer = this.argAsBestFPlayerMatch(1, fme);
            if (targetPlayer == null) return;
            added = territory.toggleFPlayer(targetPlayer);
            target = "Player \"" + targetPlayer.getName() + "\"";
        } else {
            Faction targetFaction = this.argAsFaction(1, myFaction);
            if (targetFaction == null) return;
            added = territory.toggleFaction(targetFaction);
            target = "Faction \"" + targetFaction.getTag() + "\"";
        }

        msg("<i>%s has been %s<i> the access list for this territory.", target, TextUtil.parseColor(added ? "<lime>added to" : "<rose>removed from"));
        showAccessList(territory, locFaction);
    }

    private void showAccessList(TerritoryAccess territory, Faction locFaction) {
        msg("<i>Host faction %s has %s<i> in this territory.", locFaction.getTag(), TextUtil.parseColor(territory.isHostFactionAllowed() ? "<lime>normal access" : "<rose>restricted access"));

        String players = territory.fplayerList();
        String factions = territory.factionList();

        if (factions.isEmpty())
            msg("Aucune factions n'est autoris�e dans ce cliam");
        else
            msg("Factions with explicit access: " + factions);

        if (players.isEmpty())
            msg("No players have been explicitly granted access.");
        else
            msg("Players with explicit access: " + players);
    }
}
