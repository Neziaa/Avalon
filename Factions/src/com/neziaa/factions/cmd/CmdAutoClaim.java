package com.neziaa.factions.cmd;

import com.neziaa.factions.Faction;
import com.neziaa.factions.struct.FPerm;
import com.neziaa.factions.struct.Permission;

public class CmdAutoClaim extends FCommand {
    public CmdAutoClaim() {
        super();
        this.aliases.add("autoclaim");

        this.optionalArgs.put("faction", "vous");

        this.permission = Permission.AUTOCLAIM.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        Faction forFaction = this.argAsFaction(0, myFaction);
        if (forFaction == null || forFaction == fme.getAutoClaimFor()) {
            fme.setAutoClaimFor(null);
            msg("<i>Claim automatique d�sactiver.");
            return;
        }

        if (!FPerm.TERRITORY.has(fme, forFaction, true)) return;

        fme.setAutoClaimFor(forFaction);

        msg("<i>Vous �tes en autoclaim pour <h>%s<i>.", forFaction.describeTo(fme));
        fme.attemptClaim(forFaction, me.getLocation(), true);
    }

}