package com.neziaa.factions.cmd;

import com.neziaa.factions.struct.Rel;

public class CmdRelationNeutral extends FRelationCommand {
    public CmdRelationNeutral() {
        aliases.add("neutral");
        targetRelation = Rel.NEUTRAL;
    }
}
