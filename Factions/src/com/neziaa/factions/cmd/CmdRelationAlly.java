package com.neziaa.factions.cmd;

import com.neziaa.factions.struct.Rel;

public class CmdRelationAlly extends FRelationCommand {
    public CmdRelationAlly() {
        aliases.add("ally");
        targetRelation = Rel.ALLY;
    }
}
