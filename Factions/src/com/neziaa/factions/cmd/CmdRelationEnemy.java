package com.neziaa.factions.cmd;

import com.neziaa.factions.struct.Rel;

public class CmdRelationEnemy extends FRelationCommand {
    public CmdRelationEnemy() {
        aliases.add("enemy");
        targetRelation = Rel.ENEMY;
    }
}
