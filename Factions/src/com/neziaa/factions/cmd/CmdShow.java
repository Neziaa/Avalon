package com.neziaa.factions.cmd;

import com.neziaa.factions.FPlayer;
import com.neziaa.factions.Faction;
import com.neziaa.factions.struct.Permission;
import com.neziaa.factions.struct.Rel;
import com.neziaa.factions.zcore.util.TextUtil;

import net.md_5.bungee.api.ChatColor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class CmdShow extends FCommand {
    public CmdShow() {
        this.aliases.add("show");
        this.aliases.add("who");
        this.aliases.add("f");

        this.optionalArgs.put("faction", "your");

        this.permission = Permission.SHOW.node;
        this.disableOnLock = false;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        Faction faction = myFaction;
        if (this.argIsSet(0)) {
            faction = this.argAsFaction(0);
            if (faction == null) return;
        }

        Collection<FPlayer> admins = faction.getFPlayersWhereRole(Rel.LEADER);
        Collection<FPlayer> mods = faction.getFPlayersWhereRole(Rel.OFFICER);
        Collection<FPlayer> normals = faction.getFPlayersWhereRole(Rel.MEMBER);
        Collection<FPlayer> recruits = faction.getFPlayersWhereRole(Rel.RECRUIT);

        msg(p.txt.titleize(faction.getTag(fme)));
        msg("<n>Description: <i>%s", faction.getDescription());
        
        boolean isAlliedClan = false;
        
        if(fme.getFaction().getClan() == faction.getClan()) {
        	isAlliedClan = true;
        }

        msg("<n>Clan: <i>%s", (isAlliedClan ? "�d" : "�c") + faction.getClan().getName());
        
        double powerBoost = faction.getPowerBoost();
        String boost = (powerBoost == 0.0) ? "" : (powerBoost > 0.0 ? " (bonus: " : " (malus: ") + powerBoost + ")";
        msg("<n>Land / Power / Maxpower: <i> %d/%d/%d %s", faction.getLandRounded(), faction.getPowerRounded(), faction.getPowerMaxRounded(), boost);

        String sepparator = p.txt.parse("<i>") + ", ";

        // List the relations to other factions
        Map<Rel, List<String>> relationTags = faction.getFactionTagsPerRelation(fme, true);

        sendMessage(p.txt.parse("<n>Allies: ") + TextUtil.implode(relationTags.get(Rel.ALLY), sepparator));

        // List the members...
        List<String> memberOnlineNames = new ArrayList<String>();
        List<String> memberOfflineNames = new ArrayList<String>();

        for (FPlayer follower : admins) {
            if (follower.isOnlineAndVisibleTo(me)) {
                memberOnlineNames.add(follower.getNameAndTitle(fme));
            } else {
                memberOfflineNames.add(follower.getNameAndTitle(fme));
            }
        }

        for (FPlayer follower : mods) {
            if (follower.isOnlineAndVisibleTo(me)) {
                memberOnlineNames.add(follower.getNameAndTitle(fme));
            } else {
                memberOfflineNames.add(follower.getNameAndTitle(fme));
            }
        }

        for (FPlayer follower : normals) {
            if (follower.isOnlineAndVisibleTo(me)) {
                memberOnlineNames.add(follower.getNameAndTitle(fme));
            } else {
                memberOfflineNames.add(follower.getNameAndTitle(fme));
            }
        }

        for (FPlayer follower : recruits) {
            if (follower.isOnline()) {
                memberOnlineNames.add(follower.getNameAndTitle(fme));
            } else {
                memberOfflineNames.add(follower.getNameAndTitle(fme));
            }
        }
        sendMessage(p.txt.parse("<n>Members online: ") + ChatColor.translateAlternateColorCodes('&',TextUtil.implode(memberOnlineNames, sepparator)));
        sendMessage(p.txt.parse("<n>Members offline: ") + TextUtil.implode(memberOfflineNames, sepparator));
    }

}
