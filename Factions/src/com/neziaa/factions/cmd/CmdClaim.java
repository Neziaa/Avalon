package com.neziaa.factions.cmd;

import com.neziaa.factions.Conf;
import com.neziaa.factions.FLocation;
import com.neziaa.factions.Faction;
import com.neziaa.factions.struct.Permission;
import com.neziaa.factions.util.SpiralTask;


public class CmdClaim extends FCommand {

    public CmdClaim() {
        super();
        this.aliases.add("claim");

        //this.requiredArgs.add("");
        this.optionalArgs.put("faction", "vous");
        this.optionalArgs.put("radius", "1");

        this.permission = Permission.CLAIM.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        // Read and validate input
        final Faction forFaction = this.argAsFaction(0, myFaction);
        int radius = this.argAsInt(1, 1);

        if (radius < 1) {
            msg("<b>Si vous sp�cifier un radus il doit �tre sup�rieur � 1.");
            return;
        }

        if (radius < 2) {
            // single chunk
            fme.attemptClaim(forFaction, me.getLocation(), true);
        } else {
            // radius claim
            if (!Permission.CLAIM_RADIUS.has(sender, false)) {
                msg("<b>Vous n'avez pas les permissions n�cessaire pour �xecutez cette commande.");
                return;
            }

            new SpiralTask(new FLocation(me), radius) {
                private int failCount = 0;
                private final int limit = Conf.radiusClaimFailureLimit - 1;

                @Override
                public boolean work() {
                    boolean success = fme.attemptClaim(forFaction, this.currentLocation(), true);
                    if (success)
                        failCount = 0;
                    else if (!success && failCount++ >= limit) {
                        this.stop();
                        return false;
                    }

                    return true;
                }
            };
        }
    }
}
