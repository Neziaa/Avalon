package com.neziaa.factions.cmd;

import com.neziaa.factions.P;
import com.neziaa.factions.struct.Permission;

public class CmdBypass extends FCommand {
    public CmdBypass() {
        super();
        this.aliases.add("bypass");

        this.optionalArgs.put("on/off", "flip");

        this.permission = Permission.ADMIN.node;
        this.disableOnLock = false;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        fme.setHasAdminMode(this.argAsBool(0, !fme.hasAdminMode()));

        if (fme.hasAdminMode()) {
            fme.msg("<i>Vous venez d'activer le mode admin");
            P.p.log(fme.getName() + " vient d'activer le mode admin.");
        } else {
            fme.msg("<i>Vous venez de désactiver le mode admin");
            P.p.log(fme.getName() + " vient de désactiver le mode admin");
        }
    }
}
