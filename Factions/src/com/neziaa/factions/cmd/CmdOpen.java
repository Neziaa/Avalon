package com.neziaa.factions.cmd;

import com.neziaa.factions.Conf;
import com.neziaa.factions.Faction;
import com.neziaa.factions.Factions;
import com.neziaa.factions.struct.Permission;

public class CmdOpen extends FCommand {
    public CmdOpen() {
        super();
        this.aliases.add("open");

        this.optionalArgs.put("yes/no", "flip");

        this.permission = Permission.OPEN.node;
        this.disableOnLock = false;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = true;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        // if economy is enabled, they're not on the bypass list, and this command has a cost set, make 'em pay
        if (!payForCommand(Conf.econCostOpen, "to open or close the faction", "for opening or closing the faction"))
            return;

        myFaction.setOpen(this.argAsBool(0, !myFaction.isOpen()));

        String open = myFaction.isOpen() ? "open" : "closed";

        // Inform
        myFaction.msg("%s<i> changed the faction to <h>%s<i>.", fme.describeTo(myFaction, true), open);
        for (Faction faction : Factions.i.get()) {
            if (faction == myFaction) {
                continue;
            }
            faction.msg("<i>The faction %s<i> is now %s", myFaction.getTag(faction), open);
        }
    }

}
