package com.neziaa.factions.cmd;

import com.neziaa.factions.Boards;
import com.neziaa.factions.Conf;
import com.neziaa.factions.FPlayers;
import com.neziaa.factions.Factions;
import com.neziaa.factions.struct.Permission;

public class CmdSaveAll extends FCommand {

    public CmdSaveAll() {
        super();
        this.aliases.add("saveall");
        this.aliases.add("save");

        //this.requiredArgs.add("");
        //this.optionalArgs.put("", "");

        this.permission = Permission.SAVE.node;
        this.disableOnLock = false;

        senderMustBePlayer = false;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        FPlayers.i.saveToDisc();
        Factions.i.saveToDisc();
        Boards.save();
        Conf.save();
        msg("<g>Synchronisation des factions effectuées.");
    }

}