package com.neziaa.factions.cmd;

import com.neziaa.factions.Boards;
import com.neziaa.factions.Conf;
import com.neziaa.factions.P;
import com.neziaa.factions.event.LandUnclaimAllEvent;
import com.neziaa.factions.integration.Econ;
import com.neziaa.factions.struct.Permission;

import org.bukkit.Bukkit;

public class CmdUnclaimall extends FCommand {
    public CmdUnclaimall() {
        this.aliases.add("unclaimall");
        this.aliases.add("declaimall");

        //this.requiredArgs.add("");
        //this.optionalArgs.put("", "");

        this.permission = Permission.UNCLAIM_ALL.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = true;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        if (Econ.shouldBeUsed()) {
            double refund = Econ.calculateTotalLandRefund(myFaction.getLandRounded());
            if (Conf.bankEnabled && Conf.bankFactionPaysLandCosts) {
                if (!Econ.modifyMoney(myFaction, refund, "to unclaim all faction land", "for unclaiming all faction land"))
                    return;
            } else {
                if (!Econ.modifyMoney(fme, refund, "to unclaim all faction land", "for unclaiming all faction land"))
                    return;
            }
        }

        LandUnclaimAllEvent unclaimAllEvent = new LandUnclaimAllEvent(myFaction, fme);
        Bukkit.getServer().getPluginManager().callEvent(unclaimAllEvent);
        // this event cannot be cancelled

        Boards.unclaimAll(myFaction.getId());
        myFaction.msg("%s<i> unclaimed ALL of your faction's land.", fme.describeTo(myFaction, true));

        if (Conf.logLandUnclaims)
            P.p.log(fme.getName() + " unclaimed everything for the faction: " + myFaction.getTag());
    }

}
