package com.neziaa.factions;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;

import org.bukkit.ChatColor;

import com.neziaa.factions.integration.Econ;
import com.neziaa.factions.struct.FFlag;
import com.neziaa.factions.struct.FPerm;
import com.neziaa.factions.struct.Rel;
import com.neziaa.factions.util.MiscUtil;
import com.neziaa.factions.zcore.persist.EntityCollection;
import com.neziaa.factions.zcore.util.TextUtil;

import net.minecraft.util.com.google.common.reflect.TypeToken;

/**
 * @author Neziaa
 *  17 dec. 2017
 */

public class Factions extends EntityCollection<Faction> {
	
    public static Factions i = new Factions();

    P p = P.p;

    private Factions() {
        super
             (
                Faction.class,
                new CopyOnWriteArrayList<Faction>(),
                new ConcurrentHashMap<String, Faction>(),
                new File(P.p.getDataFolder(), "factions.json"),
                P.p.gson
             );
    }

	@SuppressWarnings("serial")
	@Override
    public Type getMapType() {
        return new TypeToken<Map<String, Faction>>() {
        }.getType();
    }

    @Override
    public boolean loadFromDisc() {
        if (!super.loadFromDisc()) return false;

        //----------------------------------------------//
        // Create Default Special Factions
        //----------------------------------------------//
        if (!this.exists("0")) {
            Faction faction = this.create("0");
            faction.setTag(ChatColor.GREEN + "Nature");
            faction.setDescription(ChatColor.GREEN + "Zone non prot�g�e o� le PVP est activ�e.");
            this.setFlagsForWilderness(faction);
        }
        
        if (!this.exists("-1")) {
            Faction faction = this.create("-1");
            faction.setTag(ChatColor.GOLD + "SafeZone");
            faction.setDescription(ChatColor.GOLD + "Zone prot�g�e o� le PVP n'est pas activ�e.");

            this.setFlagsForSafeZone(faction);
        }
        
        if (!this.exists("-2")) {
            Faction faction = this.create("-2");
            faction.setTag(ChatColor.RED + "Zone de guerre");
            faction.setDescription(ChatColor.RED + "Ce n'est surement pas l'endroit le plus sur.");
            this.setFlagsForWarZone(faction);
        }
        
        /**----------------------------------------------//
         * @Author Neziaa
         * Create custom factions
         * 17 dec. 2017 - 23:14
        //----------------------------------------------**/
        
        if (!this.exists("-3")) {
            Faction faction = this.create("-3");
            faction.setTag(ChatColor.BLUE + "Staff");
            faction.setDescription(ChatColor.BLUE + "La seul faction qui ne vous attaquera pas :)");
        }
        
        if (!this.exists("-4")) {
            Faction faction = this.create("-4");
            faction.setTag(ChatColor.AQUA + "EVENT");
            faction.setDescription(ChatColor.AQUA + "Protecteur des �venement du serveur depuis 1914.");
        }

        //----------------------------------------------//
        // Fix From Old Formats
        //----------------------------------------------//
        
        Faction wild = this.get("0");
        Faction safeZone = this.get("-1");
        Faction warZone = this.get("-2");
        
        Faction staff = this.get("-3");
        Faction event = this.get("-4");

        // Set Flags if they are not set already.
        if (wild != null && !wild.getFlag(FFlag.PERMANENT))
            setFlagsForWilderness(wild);
        if (safeZone != null && !safeZone.getFlag(FFlag.PERMANENT))
            setFlagsForSafeZone(safeZone);
        if (warZone != null && !warZone.getFlag(FFlag.PERMANENT))
            setFlagsForWarZone(warZone);
        
        /**----------------------------------------------//
         * @Author Neziaa
         * Set the flag of custom faction.
         * 17 dec. 2017 - 23:24
        //----------------------------------------------**/
        
        if (staff != null && !staff.getFlag(FFlag.PERMANENT))
            setFlagsForStaff(staff);
        if (event != null && !event.getFlag(FFlag.PERMANENT))
            setFlagsForEvent(event);

        // populate all faction player lists
        for (Faction faction : i.get()) {
            faction.refreshFPlayers();
        }

        return true;
    }

    //----------------------------------------------//
    // Flag Setters
    //----------------------------------------------//
    public void setFlagsForWilderness(Faction faction) {
        faction.setOpen(false);

        faction.setFlag(FFlag.PERMANENT, true);
        faction.setFlag(FFlag.PEACEFUL, false);
        faction.setFlag(FFlag.INFPOWER, true);
        faction.setFlag(FFlag.POWERLOSS, true);
        faction.setFlag(FFlag.PVP, true);
        faction.setFlag(FFlag.FRIENDLYFIRE, false);
        faction.setFlag(FFlag.MONSTERS, true);
        faction.setFlag(FFlag.EXPLOSIONS, true);
        faction.setFlag(FFlag.FIRESPREAD, true);
        faction.setFlag(FFlag.ENDERGRIEF, true);

        faction.setPermittedRelations(FPerm.BUILD, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.DOOR, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.CONTAINER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.BUTTON, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.LEVER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
    }

    public void setFlagsForSafeZone(Faction faction) {
        faction.setOpen(false);

        faction.setFlag(FFlag.PERMANENT, true);
        faction.setFlag(FFlag.PEACEFUL, true);
        faction.setFlag(FFlag.INFPOWER, true);
        faction.setFlag(FFlag.POWERLOSS, false);
        faction.setFlag(FFlag.PVP, false);
        faction.setFlag(FFlag.FRIENDLYFIRE, false);
        faction.setFlag(FFlag.MONSTERS, false);
        faction.setFlag(FFlag.EXPLOSIONS, false);
        faction.setFlag(FFlag.FIRESPREAD, false);
        faction.setFlag(FFlag.ENDERGRIEF, false);

        faction.setPermittedRelations(FPerm.DOOR, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.CONTAINER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.BUTTON, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.LEVER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.TERRITORY, Rel.LEADER, Rel.OFFICER, Rel.MEMBER);
    }

    public void setFlagsForWarZone(Faction faction) {
        faction.setOpen(false);

        faction.setFlag(FFlag.PERMANENT, true);
        faction.setFlag(FFlag.PEACEFUL, true);
        faction.setFlag(FFlag.INFPOWER, true);
        faction.setFlag(FFlag.POWERLOSS, true);
        faction.setFlag(FFlag.PVP, true);
        faction.setFlag(FFlag.FRIENDLYFIRE, true);
        faction.setFlag(FFlag.MONSTERS, true);
        faction.setFlag(FFlag.EXPLOSIONS, true);
        faction.setFlag(FFlag.FIRESPREAD, true);
        faction.setFlag(FFlag.ENDERGRIEF, true);

        faction.setPermittedRelations(FPerm.DOOR, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.CONTAINER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.BUTTON, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.LEVER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.TERRITORY, Rel.LEADER, Rel.OFFICER, Rel.MEMBER);
    }
    
    /**----------------------------------------------//
     * @Author Neziaa
     * Create flag for custom faction
     * 17 dec. 2017 - 23:21
    //----------------------------------------------**/
    
    public void setFlagsForStaff(Faction faction) {
        faction.setOpen(false);

        faction.setFlag(FFlag.PERMANENT, true);
        faction.setFlag(FFlag.PEACEFUL, true);
        faction.setFlag(FFlag.INFPOWER, true);
        faction.setFlag(FFlag.POWERLOSS, false);
        faction.setFlag(FFlag.PVP, true);
        faction.setFlag(FFlag.FRIENDLYFIRE, false);
        faction.setFlag(FFlag.MONSTERS, false);
        faction.setFlag(FFlag.EXPLOSIONS, false);
        faction.setFlag(FFlag.FIRESPREAD, false);
        faction.setFlag(FFlag.ENDERGRIEF, false);

        faction.setPermittedRelations(FPerm.DOOR, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.CONTAINER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.BUTTON, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.LEVER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.TERRITORY, Rel.LEADER, Rel.OFFICER, Rel.MEMBER);
    }
    
    public void setFlagsForEvent(Faction faction) {
        faction.setOpen(false);

        faction.setFlag(FFlag.PERMANENT, true);
        faction.setFlag(FFlag.PEACEFUL, true);
        faction.setFlag(FFlag.INFPOWER, true);
        faction.setFlag(FFlag.POWERLOSS, false);
        faction.setFlag(FFlag.PVP, true);
        faction.setFlag(FFlag.FRIENDLYFIRE, false);
        faction.setFlag(FFlag.MONSTERS, false);
        faction.setFlag(FFlag.EXPLOSIONS, false);
        faction.setFlag(FFlag.FIRESPREAD, false);
        faction.setFlag(FFlag.ENDERGRIEF, false);

        faction.setPermittedRelations(FPerm.DOOR, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.CONTAINER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.BUTTON, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.LEVER, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT, Rel.ALLY, Rel.TRUCE, Rel.NEUTRAL, Rel.ENEMY);
        faction.setPermittedRelations(FPerm.TERRITORY, Rel.LEADER, Rel.OFFICER, Rel.MEMBER);
    }


    //----------------------------------------------//
    // GET
    //----------------------------------------------//

    @Override
    public Faction get(String id) {
        if (!this.exists(id)) {
            p.log(Level.WARNING, "La faction" + id + " n'existe pas!");
            Boards.clean();
            FPlayers.i.clean();
        }

        return super.get(id);
    }

    public Faction getNone() {
        return this.get("0");
    }
    
    /**----------------------------------------------//
     * @Author Neziaa
     * Getter of the custom faction
     * 17 dec. 2017 - 23:26
    //----------------------------------------------**/
    
    public Faction getSafezone() {
        return this.get("-1");
    }
    
    public Faction getWarzone() {
        return this.get("-2");
    }
    
    public Faction getStaff() {
        return this.get("-3");
    }
    
    public Faction getEvent() {
        return this.get("-4");
    }

    //----------------------------------------------//
    // Faction tag
    //----------------------------------------------//

    public static ArrayList<String> validateTag(String str) {
        ArrayList<String> errors = new ArrayList<String>();

        if (MiscUtil.getComparisonString(str).length() < Conf.factionTagLengthMin) {
            errors.add(P.p.txt.parse("<i>Le nom de faction ne peut pas �tre inf�rieur � <h>%s<i> caract�res.", Conf.factionTagLengthMin));
        }

        if (str.length() > Conf.factionTagLengthMax) {
            errors.add(P.p.txt.parse("<i>Le nom de faction ne peut pas �tre sup�rieur � <h>%s<i> caract�res.", Conf.factionTagLengthMax));
        }

        for (char c : str.toCharArray()) {
            if (!MiscUtil.substanceChars.contains(String.valueOf(c))) {
                errors.add(P.p.txt.parse("<i>Le nom de la faction doit contenir uniquement des lettres \"<h>%s<i>\" n'est pas une lettre.", c));
            }
        }

        return errors;
    }

    public Faction getByTag(String str) {
        String compStr = MiscUtil.getComparisonString(str);
        for (Faction faction : this.get()) {
            if (faction.getComparisonTag().equals(compStr)) {
                return faction;
            }
        }
        return null;
    }

    public Faction getBestTagMatch(String searchFor) {
        Map<String, Faction> tag2faction = new HashMap<String, Faction>();

        // TODO: Slow index building
        for (Faction faction : this.get()) {
            tag2faction.put(ChatColor.stripColor(faction.getTag()), faction);
        }

        String tag = TextUtil.getBestStartWithCI(tag2faction.keySet(), searchFor);
        if (tag == null) return null;
        return tag2faction.get(tag);
    }

    public boolean isTagTaken(String str) {
        return this.getByTag(str) != null;
    }

    public void econLandRewardRoutine() {
        if (!Econ.shouldBeUsed()) return;

        P.p.log("Running econLandRewardRoutine...");
        for (Faction faction : this.get()) {
            int landCount = faction.getLandRounded();
            if (!faction.getFlag(FFlag.PEACEFUL) && landCount > 0) {
                Set<FPlayer> players = faction.getFPlayers();
                int playerCount = players.size();
                double reward = Conf.econLandReward * landCount / playerCount;
                for (FPlayer player : players) {
                    Econ.modifyMoney(player, reward, "to own faction land", "for faction owning " + landCount + " land divided among " + playerCount + " member(s)");
                }
            }
        }
    }

}
