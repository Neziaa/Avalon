package com.neziaa.factions.listeners;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.util.NumberConversions;

import com.neziaa.factions.Boards;
import com.neziaa.factions.Conf;
import com.neziaa.factions.FLocation;
import com.neziaa.factions.FPlayer;
import com.neziaa.factions.FPlayers;
import com.neziaa.factions.Faction;
import com.neziaa.factions.P;
import com.neziaa.factions.event.FPlayerJoinEvent;
import com.neziaa.factions.event.FPlayerLeaveEvent;
import com.neziaa.factions.scoreboards.FScoreboard;
import com.neziaa.factions.scoreboards.FTeamWrapper;
import com.neziaa.factions.struct.FPerm;
import com.neziaa.factions.struct.Rel;
import com.neziaa.factions.struct.TerritoryAccess;
import com.neziaa.factions.util.VisualizeUtil;


public class FactionsPlayerListener implements Listener {
    public P p;

    public FactionsPlayerListener(P p) {
        this.p = p;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        // Make sure that all online players do have a fplayer.
        final FPlayer me = FPlayers.i.get(event.getPlayer());

        // Update the lastLoginTime for this fplayer
        me.setLastLoginTime(System.currentTimeMillis());

        // Store player's current FLocation and notify them where they are
        me.setLastStoodAt(new FLocation(event.getPlayer().getLocation()));

        // Set NoBoom timer update.
        Faction faction = me.getFaction();
        if (me.hasFaction() && Conf.protectOfflineFactionsFromExplosions) {
            //Notify our faction that the number of online players has changed.
            faction.updateLastOnlineTime();
        }

        // Send player a message
        me.sendFactionHereMessage();
        
        /**----------------------------------------------//
         * @Author Neziaa
         * When @player connecting send msg to faction
         * 18 dec. 2017 - 08:39
        //----------------------------------------------**/
        
        faction.msg("<n>[<i>%s<n>] <i>%s vient de se connecter", faction.getTag(), event.getPlayer().getName());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {
        FPlayer me = FPlayers.i.get(event.getPlayer());

        // Make sure player's power is up to date when they log off.
        me.getPower();
        // and update their last login time to point to when the logged off, for auto-remove routine
        me.setLastLoginTime(System.currentTimeMillis());

        // Set NoBoom timer update.
        Faction faction = me.getFaction();
        if (me.hasFaction() && Conf.protectOfflineFactionsFromExplosions) {
            //Notify our faction that the number of online players has changed.
            faction.updateLastOnlineTime();
        }

        /**----------------------------------------------//
         * @Author Neziaa
         * When @player connecting send msg to faction
         * 18 dec. 2017 - 08:39
        //----------------------------------------------**/
        
        faction.msg("<i> %s vient de se d�connecter!", me.getName());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.isCancelled()) return;

        // quick check to make sure player is moving between chunks; good performance boost
        if (event.getFrom().getBlockX() >> 4 == event.getTo().getBlockX() >> 4
                && event.getFrom().getBlockZ() >> 4 == event.getTo().getBlockZ() >> 4
                && event.getFrom().getWorld() == event.getTo().getWorld()) {
            return;
        }

        Player player = event.getPlayer();
        FPlayer me = FPlayers.i.get(player);

        // Did we change coord?
        FLocation from = me.getLastStoodAt();
        FLocation to = new FLocation(event.getTo());

        if (from.equals(to)) return;

        // Yes we did change coord (:

        me.setLastStoodAt(to);
        TerritoryAccess access = Boards.getTerritoryAccessAt(to);

        // Did we change "host"(faction)?
        boolean changedFaction = (Boards.getFactionAt(from) != access.getHostFaction());

        if (me.isMapAutoUpdating()) {
            me.sendMessage(Boards.getMap(me.getFaction(), to, player.getLocation().getYaw()));
        } else if (changedFaction) {
            me.sendFactionHereMessage();
        }

        // show access info message if needed
        if (!access.isDefault()) {
            if (access.subjectHasAccess(me))
                me.msg("<g>Vous avez un acc�s mod�rer ici.");
            else if (access.subjectAccessIsRestricted(me))
                me.msg("<b>Vous n'avez aucun acc�s dans ce territoire.");
        }

        if (me.getAutoClaimFor() != null) {
            me.attemptClaim(me.getAutoClaimFor(), event.getTo(), true);
        }
    }

    @SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.isCancelled()) return;
        // only need to check right-clicks and physical as of MC 1.4+; good performance boost
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.PHYSICAL) return;

        Block block = event.getClickedBlock();
        Player player = event.getPlayer();

        if (block == null) return;  // clicked in air, apparently

        if (!canPlayerUseBlock(player, block, false)) {
            event.setCancelled(true);
            if (Conf.handleExploitInteractionSpam) {
                String name = player.getName();
                InteractAttemptSpam attempt = interactSpammers.get(name);
                if (attempt == null) {
                    attempt = new InteractAttemptSpam();
                    interactSpammers.put(name, attempt);
                }
                int count = attempt.increment();
                if (count >= 10) {
                    FPlayer me = FPlayers.i.get(player);
                    me.msg("<b>Aie! Arr�te de spam!");
                    player.damage(NumberConversions.floor((double) count / 10));
                }
            }
            return;
        }

        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;  // only interested on right-clicks for below

        if (!playerCanUseItemHere(player, block.getLocation(), event.getMaterial(), false)) {
            event.setCancelled(true);
            return;
        }
    }


    // for handling people who repeatedly spam attempts to open a door (or similar) in another faction's territory
    private Map<String, InteractAttemptSpam> interactSpammers = new HashMap<String, InteractAttemptSpam>();

    private static class InteractAttemptSpam {
        private int attempts = 0;
        private long lastAttempt = System.currentTimeMillis();

        // returns the current attempt count
        public int increment() {
            long Now = System.currentTimeMillis();
            if (Now > lastAttempt + 2000)
                attempts = 1;
            else
                attempts++;
            lastAttempt = Now;
            return attempts;
        }
    }


    // TODO: Refactor ! justCheck    -> to informIfNot
    // TODO: Possibly incorporate pain build...
    @SuppressWarnings("unlikely-arg-type")
	public static boolean playerCanUseItemHere(Player player, Location loc, Material material, boolean justCheck) {
        if (Conf.playersWhoBypassAllProtection.contains(player.getUniqueId())) return true;

        FPlayer me = FPlayers.i.get(player);
        if (me.hasAdminMode()) return true;
        if (Conf.materialsEditTools.contains(material) && !FPerm.BUILD.has(me, loc, !justCheck)) return false;
        return true;
    }

    @SuppressWarnings("unlikely-arg-type")
	public static boolean canPlayerUseBlock(Player player, Block block, boolean justCheck) {
        if (Conf.playersWhoBypassAllProtection.contains(player.getUniqueId())) return true;

        FPlayer me = FPlayers.i.get(player);
        if (me.hasAdminMode()) return true;
        Location loc = block.getLocation();
        Material material = block.getType();

        if (Conf.materialsEditOnInteract.contains(material) && !FPerm.BUILD.has(me, loc, !justCheck)) return false;
        if (Conf.materialsContainer.contains(material) && !FPerm.CONTAINER.has(me, loc, !justCheck)) return false;
        if (Conf.materialsDoor.contains(material) && !FPerm.DOOR.has(me, loc, !justCheck)) return false;
        if (material == Material.STONE_BUTTON && !FPerm.BUTTON.has(me, loc, !justCheck)) return false;
        if (material == Material.LEVER && !FPerm.LEVER.has(me, loc, !justCheck)) return false;
        return true;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        FPlayer me = FPlayers.i.get(event.getPlayer());

        me.getPower();  // update power, so they won't have gained any while dead
        
        // Teleport player to spawn if is power is negatif, also player can be spawn kill and spam kill is not cool for chat :d
        
        if(me.getPower() < 0) {
        	me.msg("<i>Vous avez %d power, vous allez �tre teleporter au spawn pour evitez le spawn kill.", me.getPower());
        	return;
        }

        Location home = me.getFaction().getHome(); // TODO: WARNING FOR NPE HERE THE ORIO FOR RESPAWN SHOULD BE ASSIGNABLE FROM CONFIG.
        if (Conf.homesEnabled && Conf.homesTeleportToOnDeath && home != null &&
                (Conf.homesRespawnFromNoPowerLossWorlds || !Conf.worldsNoPowerLoss.contains(event.getPlayer().getWorld().getName()))) {
            event.setRespawnLocation(home);
        }
    }

    // For some reason onPlayerInteract() sometimes misses bucket events depending on distance (something like 2-3 blocks away isn't detected),
    // but these separate bucket events below always fire without fail
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        if (event.isCancelled()) return;

        Block block = event.getBlockClicked();
        Player player = event.getPlayer();

        if (!playerCanUseItemHere(player, block.getLocation(), event.getBucket(), false)) {
            event.setCancelled(true);
            return;
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
        if (event.isCancelled()) return;

        Block block = event.getBlockClicked();
        Player player = event.getPlayer();

        if (!playerCanUseItemHere(player, block.getLocation(), event.getBucket(), false)) {
            event.setCancelled(true);
            return;
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerKick(PlayerKickEvent event) {
        if (event.isCancelled()) return;

        FPlayer badGuy = FPlayers.i.get(event.getPlayer());
        if (badGuy == null) {
            return;
        }

        FScoreboard.remove(badGuy);

        // if player was banned (not just kicked), get rid of their stored info
        if (Conf.removePlayerDataWhenBanned && event.getReason().equals("Banned by admin.")) {
            if (badGuy.getRole() == Rel.LEADER)
                badGuy.getFaction().promoteNewLeader();
            badGuy.leave(false);
            badGuy.detach();
        }
    }

    // -------------------------------------------- //
    // VisualizeUtil
    // -------------------------------------------- //

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerMoveClearVisualizations(PlayerMoveEvent event) {
        if (event.isCancelled()) return;

        Block blockFrom = event.getFrom().getBlock();
        Block blockTo = event.getTo().getBlock();
        if (blockFrom.equals(blockTo)) return;

        VisualizeUtil.clear(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    final public void onFactionJoin(FPlayerJoinEvent event) {
        FTeamWrapper.applyUpdatesLater(event.getFaction());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFactionLeave(FPlayerLeaveEvent event) {
        FTeamWrapper.applyUpdatesLater(event.getFaction());
    }
}
