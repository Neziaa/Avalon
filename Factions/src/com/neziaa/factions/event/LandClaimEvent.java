package com.neziaa.factions.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.neziaa.factions.FLocation;
import com.neziaa.factions.FPlayer;
import com.neziaa.factions.Faction;

import lombok.Getter;

public class LandClaimEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    
    private boolean cancelled;
    @Getter private FLocation location;
    @Getter private Faction faction;
    @Getter private FPlayer fplayer;

    public LandClaimEvent(FLocation loc, Faction f, FPlayer p) {
        cancelled = false;
        location = loc;
        faction = f;
        fplayer = p;
    }
    
    @Override
	public HandlerList getHandlers() {
		return handlers;
	}

    public static HandlerList getHandlerList() {
        return handlers;
    }
    public String getFactionId() {
        return faction.getId();
    }

    public String getFactionTag() {
        return faction.getTag();
    }

    public Player getPlayer() {
        return fplayer.getPlayer();
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean c) {
        this.cancelled = c;
    }
}
