package com.neziaa.factions.scoreboards;

import java.util.List;

import com.neziaa.factions.FPlayer;

public abstract class FSidebarProvider {
    public abstract String getTitle(FPlayer fplayer);

    public abstract List<String> getLines(FPlayer fplayer);
}