package com.neziaa.base.profile;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import com.neziaa.base.Base;
import com.neziaa.base.utils.json.DiscUtil;
import com.neziaa.base.utils.json.JsonPersist;

import lombok.Getter;
import net.minecraft.util.com.google.gson.reflect.TypeToken;

public class ProfileManager implements JsonPersist {
	
	@Getter Map<String, Profile> profiles = new HashMap<String, Profile>();
	
	@Override
	public File getFile() {
		return new File(Base.i.getDataFolder(), "players.json");
	}

	@Override
	public void loadData() {
		long timeEnableStart = System.currentTimeMillis();
		String content = DiscUtil.readCatch(getFile());
		if (content == null) return;
		
		Type type = new TypeToken<Map<String, Profile>>(){}.getType();
		Map<String, Profile> map =  JsonPersist.gson.fromJson(content, type);
		
		profiles.clear();
		profiles.putAll(map);
		
		Base.i.log("&d" + profiles.size() + " &bprofile loaded (&d" + (System.currentTimeMillis() - timeEnableStart) + "ms&b).");
	}

	@Override
	public void saveData(boolean sync) {
		DiscUtil.writeCatch(getFile(), JsonPersist.gson.toJson(profiles), true);	
	}
	
	public Profile getProfile(Player player) {
		return this.profiles.get(player.getName());
	}

}
