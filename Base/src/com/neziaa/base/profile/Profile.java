package com.neziaa.base.profile;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.neziaa.base.Base;
import com.neziaa.base.settings.Settings;
import com.neziaa.base.settings.SettingsManager;
import com.neziaa.base.utils.Utils;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Profile {
	
	/** 
	 *        � Informations � 
	 * ID -> ID unique
	 * Name -> Pseudo du joueur
	 * UUID -> Pour get les factions apr�s tout?
	 * CustomName -> Pour le /nick
	 * Date -> Bah ou�, savoir quands le mec est arriv� c'est bien nan? -> Commit: update to long ? :)
	 * 
	 *         � Essentials �
	 * Home -> Home (5 max)
	 * Settings -> lstb -> soon 
	 **/
	
	private Integer id;
	private String name;
	private UUID uuid;
	private String customName;
	private long created;

	private Map<String, Location> homes = new HashMap<String, Location>();
	private Map<String, Long> kits = new HashMap<String, Long>();
	private Integer money;
	
	private Settings settings;
	
	/**
	 * Profile is not created once. We create the profile
	 * @param player
	 */
	
	public Profile(Player player) {
		this.id = (Base.getProfileManager().getProfiles().size() + 1); 
		this.name = player.getName(); 
		this.uuid = player.getUniqueId(); 
		this.customName = null;
		this.created = System.currentTimeMillis();
		
		this.homes = new HashMap<String, Location>();
		this.kits = new HashMap<String, Long>();
		this.money = 100;
		
		this.settings = new Settings(false, 0);
	}
	
	public void message(String message) {
		this.getPlayer().sendMessage(Utils.color(message));
		return;
	}
	
	public Player getPlayer() {
		return Bukkit.getPlayer(name);
	}
	
	public boolean isNick() {
		if(this.customName == null) return false;
		if(this.customName.equalsIgnoreCase(" ")) return false;
		if(this.customName.equalsIgnoreCase(this.getName())) return false;
		return true;
	}
	
	
}
