package com.neziaa.base.configuration;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.Bukkit;

import com.neziaa.base.Base;
import com.neziaa.base.utils.Saveable;
import com.neziaa.base.utils.json.DiscUtil;

import net.minecraft.util.com.google.gson.reflect.TypeToken;

public class ConfigurationManager implements Saveable {

	private Configuration configuration;
	private TypeToken<Configuration> type = new TypeToken<Configuration>() {};

	public File getFile() {
		return new File(Base.i.getDataFolder(), "configuration.json");
	}
	
	@Override
	public void load() {
		long timeEnableStart = System.currentTimeMillis();
		String content = DiscUtil.readCatch(this.getFile());
		
		if (content == null) {
			return;
		}
		
		try {
			this.configuration = Base.i.getGson().fromJson(content, type.getType());
		} catch (Exception ex) {
			Bukkit.getLogger().log(Level.SEVERE, "Probl�me de configuration... " + ex);
		}
		
		Base.i.log("&bConfiguration loaded. (&d" + (System.currentTimeMillis() - timeEnableStart) + "ms&b).");
	}

	@Override
	public void save(boolean async) {
		DiscUtil.writeCatch(getFile(), Base.i.getGson().toJson(this.getConfiguration()), true);
	}
	
	public Configuration getConfiguration() {
		if (this.configuration == null)
			return this.configuration = new Configuration();
		return this.configuration;
	}

}
