package com.neziaa.base.configuration;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
public class Configuration {

	private String host = "localhost";
	private String port = "3307";
	private String database = "factions";
	private String username = "factions";
	private String password = "factions";
	
	
}
