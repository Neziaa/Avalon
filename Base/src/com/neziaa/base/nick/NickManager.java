package com.neziaa.base.nick;

import java.util.HashMap;
import java.util.Map;

public class NickManager {
	
	private Map<String, Boolean> nicks = new HashMap<String, Boolean>();
	
	public void registerNick() {
		this.nicks.put("momomow", false);
		this.nicks.put("Shakkle", false);
		this.nicks.put("Jarzki", false);
	}
	
	public boolean isUsed(String nick) {
		return this.nicks.get(nick);
	}
	

}
