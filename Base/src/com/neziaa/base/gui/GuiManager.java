package com.neziaa.base.gui;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import com.neziaa.base.Base;

public class GuiManager {
	
    private final ConcurrentMap<UUID, AbstractGui> playersGui;

    public GuiManager(Base base) {
        this.playersGui = new ConcurrentHashMap<>();
    }

    public void onLogout(Player player)
    {
        if (this.playersGui.containsKey(player.getUniqueId()))
        {
            this.playersGui.get(player.getUniqueId()).onClose(player);
            this.playersGui.remove(player.getUniqueId());
        }
    }

    public void openGui(Player player, AbstractGui gui)
    {
        if (this.playersGui.containsKey(player.getUniqueId()))
        {
            player.closeInventory();
            this.playersGui.remove(player.getUniqueId());
        }

        this.playersGui.put(player.getUniqueId(), gui);
        gui.display(player);
    }

    public void closeGui(Player player)
    {
        if (this.playersGui.containsKey(player.getUniqueId()))
        {
            player.closeInventory();
            this.playersGui.remove(player.getUniqueId());
        }
    }

    public void removeClosedGui(Player player)
    {
        if (this.playersGui.containsKey(player.getUniqueId()))
            this.playersGui.remove(player.getUniqueId());
    }

    public AbstractGui getPlayerGui(HumanEntity humanEntity)
    {
        return this.getPlayerGui(humanEntity.getUniqueId());
    }

    public AbstractGui getPlayerGui(UUID uuid)
    {
        return this.playersGui.containsKey(uuid) ? this.playersGui.get(uuid) : null;
    }

    public ConcurrentHashMap<UUID, AbstractGui> getPlayersGui(){
        return (ConcurrentHashMap<UUID, AbstractGui>) this.playersGui;
    }
}
