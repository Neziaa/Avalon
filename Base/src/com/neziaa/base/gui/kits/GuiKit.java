package com.neziaa.base.gui.kits;

import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.neziaa.base.Base;
import com.neziaa.base.gui.AbstractGui;
import com.neziaa.base.kit.Kit;
import com.neziaa.base.kit.KitUtils;
import com.neziaa.base.profile.Profile;
import com.neziaa.base.utils.ItemBuilder;
import com.neziaa.base.utils.TimeUtils;

public class GuiKit extends AbstractGui {

	@Override
	public void display(Player player) {
		this.inventory = Base.i.getServer().createInventory(null, 54, "�9Kits");
        
        this.setSlotData(getBackIcon(), this.inventory.getSize() - 5, "back");

        player.openInventory(this.inventory);
		
	}
	
	@Override
	public void onClick(Player player, ItemStack stack, String action, ClickType clickType) {
		switch(action) {
			
			case "novice":
				Bukkit.dispatchCommand((CommandSender) player, "kit novice");
				Base.i.getGuiManager().closeGui(player);
				break;
				
			case "back":
				Base.i.getGuiManager().closeGui(player);
                break;
		}
	}
	
	public enum KitStatus {
		
		NOT_BUY,
		
		COOLDOWN,
		
		ACCESS,
	}

}
