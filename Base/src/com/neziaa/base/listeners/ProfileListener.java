package com.neziaa.base.listeners;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;
import com.neziaa.base.Base;
import com.neziaa.base.profile.Profile;
import com.neziaa.base.utils.Cuboid;
import com.neziaa.base.utils.Utils;

import net.md_5.bungee.api.ChatColor;

public class ProfileListener implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		if(Base.getProfileManager().getProfile(event.getPlayer()) == null) {
			Profile profile = new Profile(event.getPlayer());
			Base.getProfileManager().getProfiles().put(event.getPlayer().getName(), profile);
			event.setJoinMessage(Utils.color("&7Bienvenue � &9" + event.getPlayer().getName() + " &7sur le serveur &9Lobby &8[&c" + profile.getId() + "&8]" ));
			return;
		}
		
		Profile profile = Base.getProfileManager().getProfile(event.getPlayer());
		
		if(profile.isNick()) {
			event.setJoinMessage(Utils.color("&8[&a+&8] �a" + profile.getCustomName()));
			return;
		}
		
		event.setJoinMessage(Utils.color("&8[&a+&8] �a" + event.getPlayer().getName()));
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Profile profile = Base.getProfileManager().getProfile(event.getPlayer());
		
		String prefix = ""; //PermissionsEx.getUser(event.getPlayer()).getGroup().getPrefix();
		
		event.setFormat(Utils.color(prefix + profile.getName() + " &8� &7" + event.getMessage()));
	}
	
	@EventHandler
	public void PlayerInteractEvent(PlayerInteractEvent event) {
		if(!event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
			return;
		}
		
		if(event.getItem().getType().equals(Material.STICK)) {
			int ratio = 2;
			
			if(event.getItem().getEnchantments().containsKey(Enchantment.LOOT_BONUS_BLOCKS)) {
				ratio = 5;
			}
			
			Location loc1 = new Location(event.getPlayer().getWorld(), event.getPlayer().getLocation().getX() - ratio, event.getPlayer().getLocation().getY() - ratio, event.getPlayer().getLocation().getZ() - ratio);
			Location loc2 = new Location(event.getPlayer().getWorld(), event.getPlayer().getLocation().getX() + ratio, event.getPlayer().getLocation().getY() + ratio, event.getPlayer().getLocation().getZ() + ratio);
			
			Cuboid cube = new Cuboid(loc1, loc2);
			
			int changed = 0;
			
			for(Block block: cube.getBlocks()) {
				if(block.getType().equals(Material.OBSIDIAN)) {
					block.setType(Material.COBBLESTONE);
					changed++;
				}
			}
			
			ItemStack it = event.getItem();
			ItemMeta im = it.getItemMeta();
			im.setDisplayName("�7Sceptre �bTemporel �7(�c1/6�7)");
			List<String> lores = Lists.newArrayList();
			lores.add("�7Sceptre �bTemporel");
			lores.add("�c1 utilisation restante!");
			im.setLore(lores);
			it.setItemMeta(im);
			
			event.getPlayer().getInventory().setItem(0, it);
			event.getPlayer().sendMessage(ChatColor.BLUE + "" +  changed + " blocks changer.");
			
		}
	}

}
