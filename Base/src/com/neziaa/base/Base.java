package com.neziaa.base;

import java.lang.reflect.Modifier;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import com.google.common.collect.Lists;
import com.neziaa.base.command.HomeCommand;
import com.neziaa.base.command.KitCommand;
import com.neziaa.base.command.NickCommand;
import com.neziaa.base.command.ProfileCommand;
import com.neziaa.base.configuration.Configuration;
import com.neziaa.base.configuration.ConfigurationManager;
import com.neziaa.base.gui.GuiManager;
import com.neziaa.base.kit.KitManager;
import com.neziaa.base.listeners.GuiListener;
import com.neziaa.base.listeners.ProfileListener;
import com.neziaa.base.nick.NickManager;
import com.neziaa.base.profile.ProfileManager;
import com.neziaa.base.settings.Settings;
import com.neziaa.base.utils.AutoSaveTask;
import com.neziaa.base.utils.Saveable;
import com.neziaa.base.utils.Utils;
import com.neziaa.base.utils.command.CommandFramework;
import com.neziaa.base.utils.json.JsonPersist;
import com.neziaa.base.utils.json.adapter.ItemStackAdapter;
import com.neziaa.base.utils.json.adapter.LocationAdapter;
import com.neziaa.base.utils.json.adapter.PotionEffectAdapter;
import com.neziaa.base.utils.json.adapter.SettingsAdapter;

import lombok.Getter;
import net.minecraft.util.com.google.gson.Gson;
import net.minecraft.util.com.google.gson.GsonBuilder;

public class Base extends JavaPlugin {
	public static Base i;
	@Getter private Gson gson;
	@Getter private CommandFramework framework;
	
	@Getter private static ConfigurationManager configurationManager;
	
	@Getter private GuiManager guiManager;
	
	@Getter public static ProfileManager profileManager;
	@Getter public static KitManager kitManager;

	@Getter private static List<Saveable> saveables = Lists.newArrayList();
	@Getter private static List<JsonPersist> persistances = Lists.newArrayList();
	
	public void onEnable() {
		long timeEnableStart = System.currentTimeMillis();
		log("&7--------- &bSTARTING &7---------");
		log("&7Starting base plugins...");

		i = this;
		gson = this.getGsonBuilder().create();
		this.getDataFolder().mkdir();
		
		/**
		 * Saveables 
		 */
		configurationManager = new ConfigurationManager();
		
		saveables.add(configurationManager);
		saveables.forEach(p -> p.load());
		
		/**
		 * Persistances 
		 */
		
		profileManager = new ProfileManager();
		kitManager = new KitManager();
		
		persistances.add(profileManager);
		persistances.add(kitManager);
		persistances.forEach(p -> p.loadData());
		
		/**
		 * Listeners
		 */
		
		this.guiManager = new GuiManager(this);
		
		Bukkit.getServer().getPluginManager().registerEvents(new ProfileListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new GuiListener(guiManager), this);
		/** 
		 * Commands 
		 */
		this.framework = new CommandFramework(this);
		framework.registerCommands(new ProfileCommand());
		framework.registerCommands(new NickCommand());
		framework.registerCommands(new HomeCommand());
		framework.registerCommands(new KitCommand());
		
		/**
		 * post initialisation
		 */
		
		new NickManager().registerNick();
		
		new AutoSaveTask().runTaskTimer(this, 60 * 20L, 1800 * 20L);
		
		log("&bBase succesfully loaded. (&d" + (System.currentTimeMillis() - timeEnableStart) + "ms&b).");
		
	}
	
	public void onDisable() {
		persistances.forEach(p -> p.saveData(true));
		saveables.forEach(p -> p.save(true));
	}
	
	
	public void log(String message) {
		Bukkit.getConsoleSender().sendMessage(Utils.color(message));
	}
	
	private GsonBuilder getGsonBuilder() {
        return new GsonBuilder()
        		.setPrettyPrinting()
        		.disableHtmlEscaping()
        		.serializeNulls()
        		.excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE)
        		.setDateFormat("dd/MM/yyyy HH:mm")
        		.registerTypeHierarchyAdapter(ItemStack.class, new ItemStackAdapter())
        		.registerTypeAdapter(PotionEffect.class, new PotionEffectAdapter())
        		.registerTypeAdapter(Settings.class, new SettingsAdapter())
        		.registerTypeAdapter(Location.class, new LocationAdapter());
    }
	
	public Configuration getConfiguration() {
		return configurationManager.getConfiguration();
	}
}
