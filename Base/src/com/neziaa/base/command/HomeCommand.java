package com.neziaa.base.command;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import com.neziaa.base.Base;
import com.neziaa.base.profile.Profile;
import com.neziaa.base.utils.Utils;
import com.neziaa.base.utils.command.Args;
import com.neziaa.base.utils.command.Command;

public class HomeCommand {
	
	@Command(name = "home",
			 permission = "base.home",
			 inGameOnly = true)
	public void onCommand(Args args) {
		Profile profile = Base.getProfileManager().getProfile(args.getPlayer());
		
		if((profile.getHomes().isEmpty()) || profile.getHomes() == null) {
			profile.message("&cVous n'avez aucun home d�finis! Tape /sethome <nom> pour en d�finir un.");
			return;
		}
		
		if(args.getArgs().length == 1) {
			if(profile.getHomes().containsKey(args.getArgs(0))) {
				Location home = profile.getHomes().get(args.getArgs(0));
				profile.message("&7Ne bougez pas! &9T�l�portation &7en cours..");
				args.getPlayer().teleport(home);
				return;
			}
			
			profile.message("&cVous n'avez pas de home se nommant &c\"&d" + args.getArgs(0) + "&c\"&c.");
			return;
		}
		
		ArrayList<String> home = new ArrayList<String>();
		
		for(String map: profile.getHomes().keySet()) {
			home.add(ChatColor.stripColor(map));
		}
		
		profile.message("Homes (" + profile.getHomes().size() + ") : " + Utils.implode(home, ", "));
		
	}
	
	@Command(name = "sethome",
			 permission = "base.home.set",
			 requiredArgs = 1,
			 inGameOnly = true)
	public void CommandSetHome(Args args) {
		Profile profile = Base.getProfileManager().getProfile(args.getPlayer());
		if(args.getArgs(0).contains("&")) {
			profile.message("&cUtilisation : /sethome <nom>");
			return;
		}
		
		profile.getHomes().put(args.getArgs(0), args.getPlayer().getLocation());
		profile.message("&7Vous venez de d�finir le home &9" + args.getArgs(0) + "&7.");
	}
	
	@Command(name = "delhome",
			 permission = "base.home.del",
			 requiredArgs = 1,
			 inGameOnly = true)
	public void CommandDelHome(Args args) {
		Profile profile = Base.getProfileManager().getProfile(args.getPlayer());
		
		if(profile.getHomes().containsKey(args.getArgs(0))) {
			profile.getHomes().remove(args.getArgs(0));
			profile.message("&7Vous venez de supprimer le home &7\"&9" + args.getArgs(0) + "&7\"&7.");
			return;
		}
		
		profile.message("&cVous n'avez pas de home se nommant &c\"&d" + args.getArgs(0) + "&c\"&c.");
	}
	

}
