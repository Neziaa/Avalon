package com.neziaa.base.command;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.neziaa.base.Base;
import com.neziaa.base.gui.kits.GuiKit;
import com.neziaa.base.kit.Kit;
import com.neziaa.base.kit.KitUtils;
import com.neziaa.base.profile.Profile;
import com.neziaa.base.utils.Utils;
import com.neziaa.base.utils.command.Args;
import com.neziaa.base.utils.command.Command;


public class KitCommand {
	
	private String PREFIX = Utils.color("&8[&9Kit&8] &7");
	
	@Command(name = "kit",
			 permission = "core.kit",
			 description = "Open the inventory of Kit",
			 usage = "/kit",
			 inGameOnly = true)
	public void onCommand(Args args) {
		Profile profile = Base.getProfileManager().getProfile(args.getPlayer());
		
		if(args.getArgs().length < 1) {
			if(Base.getKitManager().getKits().isEmpty()) {
				args.getSender().sendMessage("�cAucun kit existant.");
				return;
			}
			
			args.getSender().sendMessage("Kits: " + Utils.implode(KitUtils.getKitNameAvailable(args.getPlayer()), ", "));
			return;
			
		}
		if(Base.getKitManager().getKits().isEmpty()) {
			args.getSender().sendMessage("�cIl n'y a aucun kits!");
			return;
		}
		
		for(Kit kits: Base.getKitManager().getKits()) {
			if(args.getArgs(0).equalsIgnoreCase(kits.getName())) {
				kits.give(args.getPlayer());
				return;
			}
		}
	}
	
	@Command(name = "kit.create",
			 permission = "core.kit.admin",
			 description = "Create a kit with the inventory of the @sender.",
			 usage = "/kit create <name> <temps>",
			 inGameOnly = true)
	public void CreateCommand(Args args) {
		if(!(args.getArgs().length == 2)) {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /kit create <name> <temps>"));
			return;
		}
		
		String name = args.getArgs(0);
		Player player = args.getPlayer();
		ArrayList<ItemStack> items = new ArrayList<ItemStack>();
		
		if(Base.getKitManager().getKits().isEmpty()) {
			try {
				int temps = Integer.parseInt(args.getArgs(1));
				
				for(ItemStack it: player.getInventory().getContents()) {
					if(!(it == null)) {
						items.add(it);
					}
				}
				
				new Kit(name, items, temps).create();
					
				args.getSender().sendMessage(Utils.color(PREFIX + "�7Vous venez de cr�er le kit '&9" + name + "&7'!"));
				return;
			} catch (NumberFormatException e) {
				args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /kit create <name> <temps>"));
				return;
			}	
		}
		
		for (Kit kit : Base.getKitManager().getKits()) {
			if (kit.getName().equalsIgnoreCase(name)) {
				args.getSender().sendMessage(Utils.color(PREFIX + "�cUn kit porte d�j� le nom &7'&4" + name + "&7'&c!"));
				return;
			}
			try {
				int temps = Integer.parseInt(args.getArgs(1));
				
				for(ItemStack it: player.getInventory().getContents()) {
					if(!(it == null)) {
						items.add(it);
					}
				}
				
				new Kit(name, items, temps).create();
					
				args.getSender().sendMessage(Utils.color(PREFIX + "�7Vous venez de cr�er le kit '&9" + name + "&7'!"));
				return;
			} catch (NumberFormatException e) {
				args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /kit create <name> <temps en heure>"));
				return;
			}
		}
	}
	
	@Command(name = "kit.delete",
			 permission = "core.kit.admin",
			 description = "Delete a kit",
			 usage = "/kit delete <name>",
			 inGameOnly = false)
	public void DeleteCommand(Args args) {
		if(!(args.getArgs().length == 1)) {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /kit delete <name>"));
			return;
		}
		
		if(Base.getKitManager().getKits().isEmpty()) {
			return;
		}
		
		for (Kit kit : Base.getKitManager().getKits()) {
			if(kit.getName().equalsIgnoreCase(args.getArgs(0))) {
				kit.delete();
				args.getSender().sendMessage(Utils.color(PREFIX + "�7Vous venez de delete le kit '&4" + args.getArgs(0) + "&7'!"));
				return;
			}
		}
	}
	
	@Command(name = "kit.nz",
			permission = "rank.joueur")
	public void test(Args args) {
		Base.i.getGuiManager().openGui(args.getPlayer(), new GuiKit());
	}
	
	public void giveKit(Player player, Kit kit) {
		
	}
}
