package com.neziaa.base.warps;

import org.bukkit.Location;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class Warp {
	
	private int id;
	private String name;
	private String description;
	private Location location;
	private String permission;
	private boolean access;

}
