package com.neziaa.base.report;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import com.neziaa.base.Base;
import com.neziaa.base.utils.json.DiscUtil;
import com.neziaa.base.utils.json.JsonPersist;

import lombok.Getter;
import net.minecraft.util.com.google.gson.reflect.TypeToken;

public class ReportManager implements JsonPersist {
	
	@Getter Map<String, Report> reports = new HashMap<String, Report>();

	@Override
	public File getFile() {
		return new File(Base.i.getDataFolder(), "reports.json");
	}

	@Override
	public void loadData() {
		String content = DiscUtil.readCatch(getFile());
		if (content == null) return;
		
		Type type = new TypeToken<Map<String, Report>>(){}.getType();
		Map<String, Report> map =  JsonPersist.gson.fromJson(content, type);
		
		reports.clear();
		reports.putAll(map);
	}

	@Override
	public void saveData(boolean sync) {
		DiscUtil.writeCatch(this.getFile(), JsonPersist.gson.toJson(reports), true);
	}
	
	public ArrayList<Report> getPlayerReports(String player){
		ArrayList<Report> list = new ArrayList<Report>();
		
		
		
		return list;
	}

}
