package com.neziaa.base.kit;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Player;

import com.neziaa.base.Base;
import com.neziaa.base.profile.Profile;

public class KitUtils {
	
	public static ArrayList<Kit> getKitAvailable(Player player) {
		ArrayList<Kit> list = new ArrayList<Kit>();
		
		for(Kit kit: Base.getKitManager().getKits()) {
			if(isAvalaible(player, kit)) {
				list.add(kit);
			}
		}
		return list;
	}
	
	public static ArrayList<String> getKitNameAvailable(Player player) {
		ArrayList<String> list = new ArrayList<String>();
		
		for(Kit kit: Base.getKitManager().getKits()) {
			if(isAvalaible(player, kit)) {
				list.add(kit.getName());
			}
		}
		return list;
	}
	
	public static boolean hasKit(Player player, Kit kit) { return player.hasPermission(kit.getPermission()); }
	
	public static boolean isCooldown(Player player, Kit kit) {
		Profile profile = Base.getProfileManager().getProfile(player);
		long kitDelay = TimeUnit.HOURS.toMillis(kit.getTime()); 

		long lastUse = 0;
		if(profile.getKits().get(kit.getName()) == null) {
			lastUse = 0;
		} else {
			lastUse = profile.getKits().get(kit.getName());
		}
		if(System.currentTimeMillis() >= lastUse + kitDelay) {
			return false;
		} 
		
		return true;
	}
	
	public static boolean isAvalaible(Player player, Kit kit) {
		Profile profile = Base.getProfileManager().getProfile(player);
		if(hasKit(player, kit)) {
			long kitDelay = TimeUnit.HOURS.toMillis(kit.getTime()); 

			long lastUse = 0;
			if(profile.getKits().get(kit.getName()) == null) {
				lastUse = 0;
			} else {
				lastUse = profile.getKits().get(kit.getName());
			}
			
			long cantUse = System.currentTimeMillis() + TimeUnit.HOURS.toMillis(kit.getTime());
//			
//			Bukkit.broadcastMessage("�bIl est " + TimeUtils.getDurationDate(System.currentTimeMillis()));
//			Bukkit.broadcastMessage("�bLe kit � un d�lais de " + kit.getTime() + " heures (" + TimeUtils.getDurationDate(kit.getTime()) + ")");
//			Bukkit.broadcastMessage("�bOn peut l'use � : �9" + TimeUtils.getDurationDate(lastUse + kitDelay));
//			Bukkit.broadcastMessage("�bIl reste :" + TimeUtils.getRemaining(lastUse + kitDelay - System.currentTimeMillis(), true));
//			Bukkit.broadcastMessage("");
//			Bukkit.broadcastMessage("Can't use before : " + TimeUtils.getDurationDate(cantUse));
//			
			
			
			if(System.currentTimeMillis() >= lastUse + kitDelay) {
				return true;
			}
		}
		return false;
	}

}