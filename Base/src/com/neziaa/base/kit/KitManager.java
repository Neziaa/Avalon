package com.neziaa.base.kit;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.Bukkit;

import com.neziaa.base.Base;
import com.neziaa.base.utils.json.DiscUtil;
import com.neziaa.base.utils.json.JsonPersist;

import lombok.Getter;
import net.minecraft.util.com.google.common.reflect.TypeToken;

public class KitManager implements JsonPersist {

	@Getter private ArrayList<Kit> kits = new ArrayList<>();

	@Override
	public File getFile() {
		return new File(Base.i.getDataFolder(), "kits.json");
	}

	@Override
	public void loadData() {
		String content = DiscUtil.readCatch(getFile());
		if (content == null) return;
		
		@SuppressWarnings("serial")
		ArrayList<Kit> map =  JsonPersist.gson.fromJson(content, new TypeToken<ArrayList<Kit>>(){}.getType());
		
		kits.clear();
		kits.addAll(map);
	}

	@Override
	public void saveData(boolean sync) {
		DiscUtil.writeCatch(getFile(), JsonPersist.gson.toJson(kits), true);	
	}
	
	public Kit getbyName(String name) {
		if(this.kits.isEmpty()) {
			Bukkit.getConsoleSender().sendMessage("�cIl n'y a pas de kit cr�er!");
			return null;
		}
		
		for(Kit kit: this.kits) {
			if(kit.getName().equalsIgnoreCase(name)) {
				return kit;
			}
		}
		return null;
	}
}
