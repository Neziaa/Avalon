package com.neziaa.base.kit;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.neziaa.base.Base;
import com.neziaa.base.profile.Profile;
import com.neziaa.base.utils.TimeUtils;
import com.neziaa.base.utils.Utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class Kit {
	
	private String name;
	private ArrayList<ItemStack> items = new ArrayList<ItemStack>();
	private String permission;
	private String description;
	private Integer time;
	private Integer prix;
	
	/**
	 * Create obj Kit with param.
	 * @param name 
	 * @param items 
	 * @param time Temps en seconde..
	 */
	public Kit(String name, ArrayList<ItemStack> items, Integer time) {
		this.name = name;
		this.items = items;
		this.permission = "kit." + name.toLowerCase();
		this.description = "Description temporaire...";
		this.time = time;
		this.prix = 1000;
	}

	/**
	 * Create a kit with name, items & time. <br/>
	 * Don't forget to do edit the kits.json
	 */
	public void create() {
		Base.getKitManager().getKits().add(this);
	}
	
	/**
	 * Delete a kit.
	 */
	
	public void delete() {
		Base.getKitManager().getKits().remove(this);
	}
	
	/**
	 * Give to player the obj kit
	 * @param player
	 */
	
	public void give(Player player) {
		Profile profile = Base.getProfileManager().getProfile(player);
		profile.getKits().put(name, System.currentTimeMillis());
		player.sendMessage(Utils.color("&7Vous pourez le refaire � : " + TimeUtils.getDurationDate(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(time))));
		
		for(ItemStack item: this.items) {
			player.getInventory().addItem(item);
		}
		player.sendMessage(Utils.color("&7Vous venez de recevoir le kit &9"  + this.name));
	}
}
