package com.neziaa.base.settings;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.neziaa.base.Base;
import com.neziaa.base.utils.json.DiscUtil;
import com.neziaa.base.utils.json.JsonPersist;

import net.minecraft.util.com.google.common.reflect.TypeToken;

public class SettingsManager implements JsonPersist {
	
	private ArrayList<Settings> settings = Lists.newArrayList();

	@Override
	public File getFile() {
		return new File(Base.i.getDataFolder(), "settings.json");
	}

	@Override
	public void loadData() {
		long timeEnableStart = System.currentTimeMillis();
		String content = DiscUtil.readCatch(getFile());
		if (content == null) return;
		
		@SuppressWarnings("serial")
		ArrayList<Settings> map =  JsonPersist.gson.fromJson(content, new TypeToken<ArrayList<Settings>>(){}.getType());
		
		settings.clear();
		settings.addAll(map);
		
		Base.i.log("&bSettings loaded (&d" + (System.currentTimeMillis() - timeEnableStart) + "ms&b).");
		
	}

	@Override
	public void saveData(boolean sync) {
		DiscUtil.writeCatch(getFile(), JsonPersist.gson.toJson(settings), true);	
	}


}
