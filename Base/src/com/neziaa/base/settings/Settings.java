package com.neziaa.base.settings;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class Settings {
	
	private boolean silentJoin = false;
	private int view = 0;

}
