package com.neziaa.base.utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.time.DurationFormatUtils;

public class TimeUtils {
	
    private static final long MINUTE;
    private static final long HOUR;
    private static final long DAY;
    private static SimpleDateFormat FRENCH_DATE_FORMAT;
    public static final ThreadLocal<DecimalFormat> REMAINING_SECONDS;
    public static final ThreadLocal<DecimalFormat> REMAINING_SECONDS_TRAILING;
    
    static {
        MINUTE = TimeUnit.MINUTES.toMillis(1L);
        HOUR = TimeUnit.HOURS.toMillis(1L);
        DAY = TimeUnit.DAYS.toMillis(1L);
        FRENCH_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        REMAINING_SECONDS = new ThreadLocal<DecimalFormat>() {
            @Override
            protected DecimalFormat initialValue() {
                return new DecimalFormat("0.#");
            }
        };
        REMAINING_SECONDS_TRAILING = new ThreadLocal<DecimalFormat>() {
            @Override
            protected DecimalFormat initialValue() {
                return new DecimalFormat("0.0");
            }
        };
    }
    
    public static String getRemaining(final long millis, final boolean milliseconds) {
        return getRemaining(millis, milliseconds, true);
    }
    
    public static String getRemaining(final long duration, final boolean milliseconds, final boolean trail) {
        if (milliseconds && duration < MINUTE) {
            return String.valueOf((trail ? REMAINING_SECONDS_TRAILING : REMAINING_SECONDS).get().format(duration * 0.001)) + 's';
        }
        if (duration >= DAY) {
            return DurationFormatUtils.formatDuration(duration, "dd-HH:mm:ss");
        }
        return DurationFormatUtils.formatDuration(duration, String.valueOf((duration >= HOUR) ? "HH:" : "") + "mm:ss");
    }
    
    public static String getDurationWords(final long duration) {
        return DurationFormatUtils.formatDuration(duration, "H' heures 'm' minutes'");
    }
    
    public static String getDurationDate(final long duration) {
        return FRENCH_DATE_FORMAT.format(new Date(duration));
    }
    
    public static String getCurrentDate() {
        return FRENCH_DATE_FORMAT.format(new Date());
    }
}
