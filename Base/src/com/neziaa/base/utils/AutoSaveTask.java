package com.neziaa.base.utils;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import com.neziaa.base.Base;

import net.md_5.bungee.api.ChatColor;

public class AutoSaveTask extends BukkitRunnable {

	@Override
	public void run() {
		long start = System.currentTimeMillis();
		Bukkit.broadcastMessage(ChatColor.BLUE + "D�but de la synchronisation.");
		
		Base.getPersistances().forEach(p -> p.saveData(true));
		Base.getSaveables().forEach(s -> s.save(true));
		
		long end = (System.currentTimeMillis() - start);
		
		new BukkitRunnable() {

			@Override
			public void run() {
				
				Bukkit.broadcastMessage(ChatColor.GREEN + "Synchronisation effectu�es.. �7(�9" + end + "ms�7)");
				
			}
			
		}.runTaskLater(Base.i, 10 * 20L);
	}

}
