package com.neziaa.base.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;

public class Utils {
	
	public static String color(String message) {
		return ChatColor.translateAlternateColorCodes('&', message);
	}
	
    /**
     * Using this function you transform a delta in milliseconds
     * to a String like "2 weeks from now" or "7 days ago".
     */
    public static final long millisPerSecond = 1000;
    public static final long millisPerMinute = 60 * millisPerSecond;
    public static final long millisPerHour = 60 * millisPerMinute;
    public static final long millisPerDay = 24 * millisPerHour;
    public static final long millisPerWeek = 7 * millisPerDay;
    public static final long millisPerMonth = 31 * millisPerDay;
    public static final long millisPerYear = 365 * millisPerDay;

    public static Map<String, Long> unitMillis;

    static {
        unitMillis = new LinkedHashMap<String, Long>();
        unitMillis.put("ann�es", millisPerYear);
        unitMillis.put("mois", millisPerMonth);
        unitMillis.put("semaines", millisPerWeek);
        unitMillis.put("jours", millisPerDay);
        unitMillis.put("heures", millisPerHour);
        unitMillis.put("minutes", millisPerMinute);
        unitMillis.put("secondes", millisPerSecond);
    }

    public static String getTimeDeltaDescriptionRelNow(long millis) {
        String ret = "";

        double millisLeft = (double) Math.abs(millis);

        List<String> unitCountParts = new ArrayList<String>();
        for (Entry<String, Long> entry : unitMillis.entrySet()) {
            if (unitCountParts.size() == 3) break;
            String unitName = entry.getKey();
            long unitSize = entry.getValue();
            long unitCount = (long) Math.floor(millisLeft / unitSize);
            if (unitCount < 1) continue;
            millisLeft -= unitSize * unitCount;
            unitCountParts.add(unitCount + " " + unitName);
        }

        if (unitCountParts.size() == 0) return "maintenant";

        ret += implodeCommaAnd(unitCountParts);
        ret += " ";
        if (millis <= 0) {
            ret += "ago";
        } else {
            ret += "maintenant";
        }

        return ret;
    }
    
    public static String implodeCommaAnd(List<String> list) {
        return implodeCommaAnd(list, ", ", " and ");
    }
    
    public static String implodeCommaAnd(List<String> list, String comma, String and) {
        if (list.size() == 0) return "";
        if (list.size() == 1) return list.get(0);

        String lastItem = list.get(list.size() - 1);
        String nextToLastItem = list.get(list.size() - 2);
        String merge = nextToLastItem + and + lastItem;
        list.set(list.size() - 2, merge);
        list.remove(list.size() - 1);

        return implode(list, comma);
    }

    public static String implode(List<String> list, String glue) {
        StringBuilder ret = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                ret.append(glue);
            }
            ret.append(list.get(i));
        }
        return ret.toString();
    }

}
