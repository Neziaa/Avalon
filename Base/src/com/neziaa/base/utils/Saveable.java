package com.neziaa.base.utils;

public interface Saveable {
	
	public void load();
	
	public void save(boolean async);

}
