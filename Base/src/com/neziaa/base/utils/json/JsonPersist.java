package com.neziaa.base.utils.json;

import java.io.File;

import com.neziaa.base.Base;

import net.minecraft.util.com.google.gson.Gson;

public interface JsonPersist {
	
	public Gson gson = (Base.i.getGson());
	
	public File getFile();
	
	public void loadData();
	
	public void saveData(boolean sync);
	

}
