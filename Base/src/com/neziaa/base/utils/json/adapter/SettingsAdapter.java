package com.neziaa.base.utils.json.adapter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.neziaa.base.settings.Settings;
import com.neziaa.base.utils.json.GsonAdapter;

import net.minecraft.util.com.google.gson.stream.JsonReader;
import net.minecraft.util.com.google.gson.stream.JsonToken;
import net.minecraft.util.com.google.gson.stream.JsonWriter;

public class SettingsAdapter extends GsonAdapter<Settings>{
	
	private static String SILENTJOIN = "silentJoin";
	private static String view = "view";

	@Override
	public String getRaw(Settings settings) {
		Map<String, Object> serial = new HashMap<String, Object>();
		serial.put(SILENTJOIN, Boolean.toString(settings.isSilentJoin()));
		serial.put(view, Integer.toString(settings.getView()));
		return this.getGson().toJson(serial);
	}

	@Override
	public Settings fromRaw(String raw) {
		Map<String, Object> keys = this.getGson().fromJson(raw, seriType);
		return new Settings(Boolean.parseBoolean(SILENTJOIN), Integer.parseInt((String) keys.get(view)));
	}

	@Override
	public Settings read(JsonReader reader) throws IOException {
		if(reader.peek() == JsonToken.NULL) {
			reader.nextNull();
			return null;
		}
		return fromRaw(reader.nextString());
	}

	@Override
	public void write(JsonWriter writer, Settings settings) throws IOException {
		if(settings == null) {
			writer.nullValue();
			return;
		}
		writer.value(getRaw(settings));
	}
	
	private boolean parseBoolean(String string) {
		if(string.equalsIgnoreCase("true")) {
			return true;
		}
		return false;
	}

}
