package com.neziaa.practice.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import com.neziaa.practice.Practice;
import com.neziaa.practice.utils.Utils;

public class ServerListener implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		event.setJoinMessage(Utils.color("&8[&a+&8] &7" + event.getPlayer()));
		
		if(!event.getPlayer().hasPlayedBefore()) {
			Practice.getEloManager().createAccount(event.getPlayer().getName());
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		event.setQuitMessage(Utils.color("&8[&c-&8] &7" + event.getPlayer()));
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event) {
		event.setCancelled(true);
	}
}
