package com.neziaa.practice.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.neziaa.practice.Practice;
import com.neziaa.practice.arena.Arena;
import com.neziaa.practice.utils.Cuboid;
import com.neziaa.practice.utils.Utils;

/**
 * @author Neziaa
 *  10 dec. 2017
 */

public class InventoryListener implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	public void onClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null || e.getCurrentItem() == null)
			return;
		if (e.getClickedInventory().getTitle().equalsIgnoreCase("�9Arena")) {
			e.setCancelled(true);
			Player p = (Player) e.getWhoClicked();
			for (Arena arena : Practice.getArenaManager().getArene()) {
				if (arena.toItemStack().equals(e.getCurrentItem())) {
					Cuboid cube = new Cuboid(arena.getOne(), arena.getTwo());
					p.teleport(cube.getCenter());
					p.sendMessage(Utils.color("&8[&9Practice&8] &7Vous venez d'�tre teleporter dans l'arene &9" + arena.getName()));
					return;
				}
			}
		}
	}
	

}
