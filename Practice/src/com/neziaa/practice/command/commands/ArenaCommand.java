package com.neziaa.practice.command.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.neziaa.practice.Practice;
import com.neziaa.practice.arena.Arena;
import com.neziaa.practice.arena.ArenaInventory;
import com.neziaa.practice.command.Command;
import com.neziaa.practice.command.CommandArgs;
import com.neziaa.practice.command.Completer;
import com.neziaa.practice.utils.Cuboid;
import com.neziaa.practice.utils.Utils;
import com.sk89q.worldedit.bukkit.selections.Selection;

/**
 * @author Neziaa
 *  11 dec. 2017
 */

public class ArenaCommand {
	
	private String PREFIX = Utils.color("&8[&9Practice&8] &7");
	
	@Completer(name = "arena")
	public List<String> ArenaCompleter(CommandArgs args) {
		List<String> list = new ArrayList<String>();
		list.add("create");
		list.add("setpoints");
		list.add("setauthor");
		list.add("list");
		list.add("save");
		list.add("delete");
		return list;
	}
	
	@Command(name = "arena", 
			 permission = "arena",
			 description = "Send to @sender list of /arena <command>", 
			 usage = "/arena")
	public void MainCommand(CommandArgs args) {
		new ArenaInventory().open(args.getPlayer());
	}
	
	@Command(name = "arena.create", 
			 permission = "arena.create",
			 description = "Create arena with <name>, using WorldEdit for selection", 
			 usage = "/arena create <name>", 
			 inGameOnly = true)
	public void CreateCommand(CommandArgs args) {
		if(!(args.getArgs().length == 1)) {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /arena create <name>"));
			return;
		}
		
		if(!args.getArgs(0).isEmpty()) {
			Selection selection = Practice.getWorldEdit().getSelection(args.getPlayer());
			if (selection == null) {
				args.getSender().sendMessage(Utils.color(PREFIX + "&cVous devez selectionner deux points avec WorldEdit!"));
				return;
			}
			
			Location one = selection.getMaximumPoint();
			Location two = selection.getMinimumPoint();
			
			for (Arena arena : Practice.getArenaManager().getArene()) {
				if (arena.getName().equalsIgnoreCase(args.getArgs(0))) {
					args.getSender().sendMessage(Utils.color(PREFIX + "�cUne arene porte d�j� le nom '&4" + args.getArgs(0) + "&c'!"));
					return;
				} else {
					Practice.getArenaManager().getArene().add(new Arena(args.getArgs(0), one, two));
					args.getSender().sendMessage(Utils.color(PREFIX + "�7Vous venez de cr�er l'arene '&9" + args.getArgs(0) + "&7'!"));
					args.getSender().sendMessage(Utils.color("�cN'oubliez pas de faire /arene setpoints " + args.getArgs(0) + " !"));
					return;
				}
			}
		} else {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /arena create <name>"));
			return;
		}
	}
	
	@Command(name = "arena.setpoints", 
			 permission = "arena.edit",
			 description = "Set spawn points for one Arena", 
			 usage = "/arena setpoints <name>", 
			 inGameOnly = true)
	public void SetPointsCommand(CommandArgs args) {
		if(!(args.getArgs().length == 1)) {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /arena setpoints <name>"));
			return;
		}
		
		if(!args.getArgs(0).isEmpty()) {
			Selection selection = Practice.getWorldEdit().getSelection(args.getPlayer());
			if (selection == null) {
				args.getSender().sendMessage(Utils.color(PREFIX + "&cVous devez selectionner deux points avec WorldEdit!"));
				return;
			}
			
			Location one = selection.getMaximumPoint();
			Location two = selection.getMinimumPoint();
			
			for (Arena arena : Practice.getArenaManager().getArene()) {
				if (arena.getName().equalsIgnoreCase(args.getArgs(0))) {
					Cuboid cube = new Cuboid(arena.getOne(), arena.getTwo());
					if(cube.contains(one) && cube.contains(two)) {
						arena.setSpawnAlpha(one);
						arena.setSpawnBravo(two);
						args.getSender().sendMessage(Utils.color(PREFIX + "&7Vous venez de cr�er les spawn pour l'arene '" + args.getArgs(0) + "'!"));
						return;
					} else {
						args.getSender().sendMessage(Utils.color(PREFIX + "&cLes points de spawn doivent �tre interieur de l'arene!"));
						return;
					}
				} 
			}
			args.getSender().sendMessage(Utils.color(PREFIX + "�cL'arene '&4" + args.getArgs(0) + "&c' n'existe pas!"));
			return;
		} else {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /arena setpoints <name>"));
			return;
		}
	}
	
	@Command(name = "arena.setauthor", 
			 permission = "arena.edit",
			 description = "Set author for a Arena", 
			 usage = "/arena setauthor <name> <pseudo>")
	public void SetAuthorCommand(CommandArgs args) {
		if(!(args.getArgs().length == 2)) {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /arena setauthor <name> <pseudo>"));
			return;
		}
		
		if(!args.getArgs(0).isEmpty()) {

			for (Arena arena : Practice.getArenaManager().getArene()) {
				if (arena.getName().equalsIgnoreCase(args.getArgs(0))) {
					if(!arena.getAuthor().equalsIgnoreCase(args.getArgs(1))) {
						arena.setAuthor(args.getArgs(1));
						args.getSender().sendMessage(Utils.color(PREFIX + "�c'�4" + args.getArgs(1) + "�c' est maintenant builder de l'arene '�4" + args.getArgs(0) + "�c'."));
						return;
					} else {
						args.getSender().sendMessage(Utils.color(PREFIX + "�cLe builder '�4" + args.getArgs(1) + "�c' est d�j� builder de cette arene!"));
						return;
					}
				} 
			}
			args.getSender().sendMessage(Utils.color(PREFIX + "�cL'arene '&4" + args.getArgs(0) + "&c' n'existe pas!"));
			return;
		} else {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /arena setauthor <name> <pseudo>"));
			return;
		}
	}
	
	@Command(name = "arena.delete", 
			 permission = "arena.delete",
			 description = "Delete arena with <name>", 
			 usage = "/arena delete <name>", 
			 inGameOnly = false)
	public void RemoveCommand(CommandArgs args) {
		if(!(args.getArgs().length == 1)) {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /arena delete <name>"));
			return;
		}
		
		if(!args.getArgs(0).isEmpty()) {
			for (Arena arena : Practice.getArenaManager().getArene()) {
				if (arena.getName().equalsIgnoreCase(args.getArgs(0))) {
					Practice.getArenaManager().getArene().remove(arena);
					args.getSender().sendMessage(Utils.color(PREFIX + "�cL'arene '&4" + args.getArgs(0) + "&c' vient d'�tre supprimer!"));
					return;
				}
			}
			args.getSender().sendMessage(Utils.color(PREFIX + "�cL'arene '&4" + args.getArgs(0) + "&c' n'existe pas!"));
			return;
		} else {
			args.getSender().sendMessage(Utils.color(PREFIX + "&cUtilise /arena delete <name>"));
			return;
		}
	}
	
	@Command(name = "arena.list")
	public void ListCommand(CommandArgs args) {
		new ArenaInventory().open(args.getPlayer());
	}
	
	@Command(name = "arena.save")
	public void save(CommandArgs args) {
		Practice.getArenaManager().saveData(true);
		Bukkit.broadcastMessage("Arena save!");
	}
}
