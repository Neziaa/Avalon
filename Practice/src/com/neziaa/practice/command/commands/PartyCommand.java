package com.neziaa.practice.command.commands;

import com.neziaa.practice.command.Command;
import com.neziaa.practice.command.CommandArgs;
import com.neziaa.practice.party.Party;
import com.neziaa.practice.party.PartyManager;
import com.neziaa.practice.utils.Utils;

public class PartyCommand {
	
	@Command(name = "party",
			 permission = "practice.party",
			 description = "Main command of Party",
			 usage = "/party")
	public void onCommand(CommandArgs args) {
		if(PartyManager.getParty().isEmpty()) {
			args.getSender().sendMessage(Utils.color("&cIl n'y aucune party."));
			return;
		}
		
		for(Party party: PartyManager.getParty()) {
			int count = 1;
			args.getSender().sendMessage(Utils.color("&7ID : &9" + count + " &cleader : &e" + party.getLeader() + " (&e" + party.getPlayer().size() + " &cjoueurs)"));
			count++;
		}
		
		return;
	}
	
	@Command(name = "party.create",
			 permission = "practice.party",
			 description = "Main command of Party",
			 usage = "/party create <name>",
			 inGameOnly = true)
	public void create(CommandArgs args) {
		if(!(args.getArgs().length == 1)) {
			args.getSender().sendMessage(Utils.color("&cUtilise /party create <name>"));
			return;
		}
		
		Party party = new Party(args.getPlayer(), args.getArgs(0));
		party.create();
		
		
	}

}
