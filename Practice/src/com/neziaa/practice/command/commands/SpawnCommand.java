package com.neziaa.practice.command.commands;

import org.bukkit.Location;

import com.neziaa.practice.Practice;
import com.neziaa.practice.command.Command;
import com.neziaa.practice.command.CommandArgs;
import com.neziaa.practice.utils.Utils;
import com.neziaa.practice.utils.configuration.Configuration;

public class SpawnCommand {
	
	@Command(name = "spawn",
			 aliases = {"lobby"},
			 permission = "practice.spawn",
			 description = "",
			 usage = "/spawn",
			 inGameOnly = true)
	public void onCommand(CommandArgs args) {
		Configuration configuration = Practice.getConfigurationManager().getConfiguration();
		args.getPlayer().teleport(configuration.getSpawn());
		
		args.getSender().sendMessage(Utils.color("&8[&9Practice&8] &7Teleportation au spawn en cours..."));
		return;
	}
	
	@Command(name = "spawn.set",
			 permission = "practice.spawn.admin",
			 description = "Set the spawn.",
			 usage = "/spawn set",
			 inGameOnly = true)
	public void SetCommand(CommandArgs args) {
		Location w = args.getPlayer().getLocation();
		Configuration configuration = Practice.getConfigurationManager().getConfiguration();
		configuration.setSpawn(w);
		
		args.getSender().sendMessage(Utils.color("&8[&9Practice&8] &7Vous venez de set le spawn ici!"));
		return;
	}

}
