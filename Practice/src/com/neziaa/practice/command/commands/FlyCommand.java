package com.neziaa.practice.command.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.neziaa.practice.command.Command;
import com.neziaa.practice.command.CommandArgs;
import com.neziaa.practice.utils.Utils;

public class FlyCommand {
	
private List<Player> flying = new ArrayList<Player>();
	
	@Command(name = "fly",
			 permission = "practice.fly", 
			 description = "Set the flying to a player",
			 usage = "/fly",
			 inGameOnly = true)
	public void onCommand(CommandArgs args) {
		if(flying.contains(args.getPlayer())) {
			args.getPlayer().setAllowFlight(false);
			args.getPlayer().setFlying(false);
			flying.remove(args.getPlayer());
			
			args.getSender().sendMessage(Utils.color("&8[&9Practice&8] &7Vous venez de désactiver le &9fly&7."));
			return;
		} else {
			args.getPlayer().setAllowFlight(true);
			args.getPlayer().setFlying(true);
			flying.add(args.getPlayer());
			
			args.getSender().sendMessage(Utils.color("&8[&9Practice&8] &7Vous venez d'activer le &9fly&7."));
			return;
		}
	}

}
