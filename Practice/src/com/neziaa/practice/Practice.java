package com.neziaa.practice;

import java.lang.reflect.Modifier;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Lists;
import com.neziaa.practice.arena.ArenaManager;
import com.neziaa.practice.command.CommandFramework;
import com.neziaa.practice.command.commands.ArenaCommand;
import com.neziaa.practice.command.commands.FlyCommand;
import com.neziaa.practice.command.commands.SpawnCommand;
import com.neziaa.practice.elo.EloManager;
import com.neziaa.practice.listener.InventoryListener;
import com.neziaa.practice.listener.ServerListener;
import com.neziaa.practice.player.PlayerManager;
import com.neziaa.practice.utils.Saveable;
import com.neziaa.practice.utils.Utils;
import com.neziaa.practice.utils.configuration.ConfigurationManager;
import com.neziaa.practice.utils.json.ItemStackAdapter;
import com.neziaa.practice.utils.json.JsonPersist;
import com.neziaa.practice.utils.json.LocationAdapter;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import lombok.Getter;
import net.minecraft.util.com.google.gson.Gson;
import net.minecraft.util.com.google.gson.GsonBuilder;

public class Practice extends JavaPlugin {
	
	@Getter private static Practice instance;
	
	@Getter private Gson gson;
	@Getter private static WorldEditPlugin worldEdit;
	
	@Getter private static ArenaManager arenaManager;
	@Getter private static ConfigurationManager configurationManager;
	@Getter private static PlayerManager playerManager;
	@Getter private static EloManager eloManager;
	
	
	@Getter private List<JsonPersist> persistances = Lists.newArrayList();
	@Getter private List<Saveable> saveables = Lists.newArrayList();
	
	CommandFramework framework;
	
	public void onEnable() {
		instance = this;
		arenaManager = new ArenaManager();
		configurationManager = new ConfigurationManager();
		playerManager = new PlayerManager();
		eloManager = new EloManager();
		
		if(getServer().getPluginManager().getPlugin("WorldEdit") == null) {
			Bukkit.getConsoleSender().sendMessage(Utils.color("[Practice] &cWorldEdit can't be load!"));
			Bukkit.getServer().getPluginManager().disablePlugins();
			return;
		} 
		
		worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
		
		this.getDataFolder().mkdir();
		
		this.gson = this.getGsonBuilder().create();
		
		this.persistances.add(arenaManager);
		this.persistances.add(playerManager);
		
		this.persistances.forEach(p -> p.loadData());
		
		this.saveables.add(configurationManager);
		this.saveables.forEach(p -> p.load());
		
		/** Load MYSQL **/
	
//		MySQL.i.connect("localhost", "3307", "nz", "root", "mariadb");
//		MySQL.i.createTable("elo", "id MEDIUMINT PRIMARY KEY NOT NULL AUTO_INCREMENT", "pseudo VARCHAR(16)", "elo INTEGER");
		
		framework = new CommandFramework(this);
		framework.registerHelp();
		
		framework.registerCommands(new ArenaCommand());
		framework.registerCommands(new FlyCommand());
		framework.registerCommands(new SpawnCommand());
		
		PluginManager pm = Bukkit.getServer().getPluginManager();
		pm.registerEvents(new InventoryListener(), this);
		pm.registerEvents(new ServerListener(), this);
	}
	
	public void onDisable() {
		this.persistances.forEach(p -> p.saveData(true));
		this.saveables.forEach(p -> p.save(true));
	}
	
	private GsonBuilder getGsonBuilder() {
        return new GsonBuilder()
        		.setPrettyPrinting()
        		.disableHtmlEscaping()
        		.serializeNulls()
        		.excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE)
        		.registerTypeHierarchyAdapter(ItemStack.class, new ItemStackAdapter())
        		.registerTypeAdapter(Location.class, new LocationAdapter());
    }
}
