package com.neziaa.practice.duel;

import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class DuelRequest {
	
	/**
	 * This is the sender of the request
	 */
	@NonNull private Player sender;
	
	/**
	 * This is the receiver of the request
	 */
	@NonNull private Player receiver;
	
	/**
	 * This is the expiration delay (x * 20L)
	 */

	private Long expiration;
	

}
