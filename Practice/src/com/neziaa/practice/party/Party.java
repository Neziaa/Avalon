package com.neziaa.practice.party;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import com.neziaa.practice.player.PracticePlayer;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
public class Party {
	private String name;
	private PracticePlayer leader;
	private ArrayList<PracticePlayer> player = new ArrayList<PracticePlayer>();
	
	public Party(Player creator, String name) {
		this.leader = new PracticePlayer(creator);
		this.name = name;
		this.player.add(leader);
	}
	
	public void create() {
		PartyManager.getParty().add(this);
	}

}
