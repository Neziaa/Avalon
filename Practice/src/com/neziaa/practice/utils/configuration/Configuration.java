package com.neziaa.practice.utils.configuration;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import lombok.Data;

@Data
public class Configuration {
	
	private String host = "localhost";
	private String port = "3307";
	private String database  = "practice";
	private String user = "root";
	private String pass = "mariadb";
	
	/** --- **/
	
	private Location spawn = new Location(Bukkit.getWorld("world"), 0, 70, 0);

}
