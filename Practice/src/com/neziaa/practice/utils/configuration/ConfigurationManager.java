package com.neziaa.practice.utils.configuration;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.Bukkit;

import com.neziaa.practice.Practice;
import com.neziaa.practice.utils.Saveable;
import com.neziaa.practice.utils.json.DiscUtil;

import net.minecraft.util.com.google.gson.reflect.TypeToken;

public class ConfigurationManager implements Saveable {

	private Configuration configuration;
	private TypeToken<Configuration> type = new TypeToken<Configuration>() {};

	public File getFile() {
		return new File(Practice.getInstance().getDataFolder(), "configuration.json");
	}
	
	@Override
	public void load() {
		String content = DiscUtil.readCatch(this.getFile());
		
		if (content == null) {
			return;
		}
		
		try {
			this.configuration = Practice.getInstance().getGson().fromJson(content, type.getType());
		} catch (Exception ex) {
			Bukkit.getLogger().log(Level.SEVERE, "Probl�me de configuration... " + ex);
		}
	}

	@Override
	public void save(boolean async) {
		DiscUtil.writeCatch(getFile(), Practice.getInstance().getGson().toJson(this.getConfiguration()), true);
	}
	
	public Configuration getConfiguration() {
		if (this.configuration == null)
			return this.configuration = new Configuration();
		return this.configuration;
	}

}
