package com.neziaa.practice.utils;

public interface Saveable {
	
	/**
	 * Load data 
	 */
	public void load();
	
	/**
	 * Save data
	 * @param async
	 */
	
	public void save(boolean async);

}
