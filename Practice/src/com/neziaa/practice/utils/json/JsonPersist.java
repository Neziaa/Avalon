package com.neziaa.practice.utils.json;

import java.io.File;

import com.neziaa.practice.Practice;

import net.minecraft.util.com.google.gson.Gson;

public interface JsonPersist {
	
	public Gson gson = (Practice.getInstance().getGson());
	
	public File getFile();
	
	public void loadData();
	
	public void saveData(boolean sync);
	

}
