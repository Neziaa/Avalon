package com.neziaa.practice.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Bukkit;

public class MySQL {
	
  public static MySQL i;
  
  private Connection connection = null;
  
  public MySQL() {
    i = this;
  }
  
  /**
   * Initialise la connexion � la bdd
   * 
   * @param host		String
   * @param port		Integer
   * @param database	String
   * @param username	String
   * @param password	String
   */
  
  public boolean connect(String host, String port, String database, String username, String password) {
    try {
      Class.forName("com.mysql.jdbc.Driver");
      this.connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
      return true;
    }
    catch (SQLException e)
    {
      Bukkit.getConsoleSender().sendMessage(Utils.color("&8[&4ERROR&8] &4SQL &8� &cMauvais identifiants..."));
      System.out.println("Erreur: " + e.getMessage());
    }
    catch (ClassNotFoundException e)
    {
      System.out.println("JDBC Driver not found!");
    }
    return false;
  }
  
  /**
   * Verification si le plugins est bien connecter.
   */
  public boolean isConnected()
  {
    if (this.connection != null) {
      return true;
    }
    return false;
  }
  
  /**
   * Ferme la connexion au serveur MySQL.
   */
  public void close(){
    if (this.connection == null) {
      try
      {
        this.connection.close();
      }
      catch (SQLException e)
      {
        System.out.println("Couldn't close connection");
      }
    }
  }
  
  /**
   * Permet d'executer du code MySQL.
   * @param sql		String
   */
  
  public boolean execute(String sql)
  {
    boolean st = false;
    try
    {
      Statement statement = this.connection.createStatement();
      st = statement.execute(sql);
      statement.close();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return st;
  }
  
  /**
   * Permet d'executer du code MySQL tout en retournant un ResultSet pour accedez a des donn�es.
   * @param sql		String
   * @return 		ResultSet
   */
  
  public ResultSet executeQuery(String sql)
  {
    PreparedStatement statement = null;
    ResultSet rs = null;
    try
    {
      statement = this.connection.prepareStatement(sql);
      rs = statement.executeQuery();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return rs;
  }
  
  /**
   * Execute une update.
   * @param sql		String
   * @return		Statement
   */
  public int executeUpdate(String sql)
  {
    int st = 0;
    try
    {
      Statement statement = this.connection.createStatement();
      st = statement.executeUpdate(sql);
      statement.close();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return st;
  }
  
  /**
   * Execute du code MySQL batch.
   * @param statements		Strings
   * @param batchSize		Integer
   */
  public void executeBatch(String[] statements, int batchSize)
  {
    int count = 0;
    
    Statement statement = null;
    try
    {
      statement = this.connection.createStatement();
      for (String query : statements)
      {
        statement.addBatch(query);
        if ((batchSize != 0) && 
          (count % batchSize == 0)) {
          statement.executeBatch();
        }
        count++;
      }
      statement.executeBatch();
      statement.close();
      statement = null;
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      if (statement != null) {
        try
        {
          statement.close();
        }
        catch (SQLException localSQLException1) {}
      }
    }
    finally
    {
      if (statement != null) {
        try
        {
          statement.close();
        }
        catch (SQLException localSQLException2) {}
      }
    }
  }
  
  /**
   * Cr�er une table si elle n'existe pas.
   * @param tablename		String
   * @param values			Strings
   */
  
  public void createTable(String tablename, String... values)
  {
    String stmt = "CREATE TABLE IF NOT EXISTS " + tablename + "(";
    for (int i = 0; i < values.length; i++) {
      if (i == values.length - 1) {
        stmt = stmt.concat(values[i]);
      } else {
        stmt = stmt.concat(values[i] + ", ");
      }
    }
    stmt = stmt.concat(");");
    try
    {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(stmt);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * Supprime une table.
   * @param tablename		String
   */
  
  public void deleteTable(String tablename)
  {
    String sql = "DROP TABLE IF EXISTS " + tablename;
    try
    {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(sql);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * Remet � zero une table MYSQL.
   * @param tablename		String
   * @param values			Strings
   */
  
  public void resetTable(String tablename, String[] values)
  {
    deleteTable(tablename);
    createTable(tablename, values);
  }
  
  /**
   * Insert du code MySQL dans la table voulu.
   * @param table		String 
   * @param columns		String
   * @param values		Object
   */
  
  public void insertInto(String table, String[] columns, Object[] values)
  {
    String statement = "INSERT INTO " + table;
    
    String c = "(";
    for (int i = 0; i < columns.length; i++) {
      if (i == columns.length - 1) {
        c = c + columns[i];
      } else {
        c = c + columns[i] + ",";
      }
    }
    c = c + ")";
    
    String v = "(";
    for (int i = 0; i < values.length; i++) {
      if (i == columns.length - 1)
      {
        if ((values[i] instanceof String)) {
          v = v + "'" + values[i] + "'";
        } else {
          v = v + values[i];
        }
      }
      else if ((values[i] instanceof String)) {
        v = v + "'" + values[i] + "', ";
      } else {
        v = v + values[i] + ", ";
      }
    }
    v = v + ")";
    
    statement = statement + c + " VALUES" + v + " ON DUPLICATE KEY UPDATE ";
    for (int i = 0; i < columns.length; i++)
    {
      statement = statement + columns[i] + "=";
      if (i == columns.length - 1)
      {
        if ((values[i] instanceof String)) {
          statement = statement + "'" + values[i] + "'";
        } else {
          statement = statement + values[i];
        }
      }
      else if ((values[i] instanceof String)) {
        statement = statement + "'" + values[i] + "', ";
      } else {
        statement = statement + values[i] + ", ";
      }
    }
    statement = statement + ";";
    executeUpdate(statement);
  }
  
  /**
   * Insert du code MySQL dans la table voulu sans cl� primaire ( r�p�tition possible ).
   * @param table		String 
   * @param columns		String
   * @param values		Object
   */
  
  public void insertIntoWithoutPrimaryKey(String table, String[] columns, Object[] values)
  {
    String statement = "INSERT INTO " + table;
    
    String c = "(";
    for (int i = 0; i < columns.length; i++) {
      if (i == columns.length - 1) {
        c = c + columns[i];
      } else {
        c = c + columns[i] + ",";
      }
    }
    c = c + ")";
    
    String v = "(";
    for (int i = 0; i < values.length; i++) {
      if (i == columns.length - 1)
      {
        if ((values[i] instanceof String)) {
          v = v + "'" + values[i] + "'";
        } else {
          v = v + values[i];
        }
      }
      else if ((values[i] instanceof String)) {
        v = v + "'" + values[i] + "', ";
      } else {
        v = v + values[i] + ", ";
      }
    }
    v = v + ")";
    
    statement = statement + c + " VALUES" + v + " ON DUPLICATE KEY UPDATE ";
    for (int i = 1; i < columns.length; i++)
    {
      statement = statement + columns[i] + "=";
      if (i == columns.length - 1)
      {
        if ((values[i] instanceof String)) {
          statement = statement + "'" + values[i] + "'";
        } else {
          statement = statement + values[i];
        }
      }
      else if ((values[i] instanceof String)) {
        statement = statement + "'" + values[i] + "', ";
      } else {
        statement = statement + values[i] + ", ";
      }
    }
    statement = statement + ";";
    
    executeUpdate(statement);
  }
  
  /**
   * Permet d'avoir une colonnes 
   * @param table		String
   * @param column		String
   * @param value		String
   * @return 			ResultSet
   */
  
  public ResultSet getRowByColumn(String table, String column, String value)
  {
    ResultSet rs = executeQuery("SELECT * FROM " + table + " WHERE " + column + " = " + value);
    
    return rs;
  }
  
  /**
   * @param table String
   * @param set ResultSet
   * @param column String
   * @return
   */
  public String getStringFromRow(String table, ResultSet set, String column)
  {
    try
    {
      return set.getString(column);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return null;
  }
  
  /**
   * 
   * @param table
   * @param set
   * @param column
   * @return
   */
  
  public int getIntFromRow(String table, ResultSet set, String column)
  {
    try
    {
      return set.getInt(column);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return 0;
  }
  
  /**
   * @param table String
   * @return the number of row
   */
  
  public int getRowCount(String table)
  {
    ResultSet rs = executeQuery("SELECT * FROM " + table);
    int count = 0;
    try
    {
      while (rs.next()) {
        count++;
      }
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return count;
  }
  
  /**
   * @return Database connection
   */
  
  public Connection getConnection()
  {
    return this.connection;
  }
 
  /**
   * @param table String
   * @param column String 
   * @param value String
   * @return
   */
  
  public boolean isExist(String table, String column, String value) {
    ResultSet rs = executeQuery("SELECT * FROM " + table + " WHERE " + column + " = '" + value + "'");
    try
    {
      if(rs.next()) {
    	  return true;
      }
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return false;
  }
}
