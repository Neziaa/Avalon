package com.neziaa.practice.elo;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.neziaa.practice.utils.MySQL;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data @RequiredArgsConstructor
public class EloManager {
	
	private String tables = "elo";
	
	/** 
	 * Create account for a PracticePlayer
	 * @param player String 
	 */
	
	public void createAccount(String player) {
		MySQL.i.insertInto(tables,
				new String[] { "id", "pseudo", "elo"},
				new Object[] { null, player, 0});
	}
	
	/** 
	 * Delete account of @param player. <br />
	 * Be warn with this, it cant be undo.
	 * @param player String 
	 */
	
	public void deleteAccount(String player) {
		MySQL.i.execute("DELETE FROM " + tables + " WHERE pseudo = '" + player + "'");
	}
	
	public int getELO(String player) {
		ResultSet set = MySQL.i.executeQuery("SELECT * FROM " + tables + " WHERE pseudo = '" + player + "'");

		try {
			while(set.next()) {
				return set.getInt("elo");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
}
