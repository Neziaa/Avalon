package com.neziaa.practice.elo;

import lombok.Getter;

@Getter
public enum EloEnum {
	
	SILVER(0, "Silver", 1150, 1300),
	BRONZE(1, "Bronze", 1300, 1450),
	GOLD(2, "Gold", 1450, 1600),
	PLATINE(3, "Platine", 1600, 1750),
	DIAMOND(4, "Diamond", 1750, 2000),
	DIEU(5, "Dieu", 2000, 2500);
	
	private Integer rank;
	private String  name;
    private Integer min;   
    private Integer max;  
    
    private EloEnum(Integer rank, String name, Integer min, Integer max) {
    	this.rank = rank;
        this.name = name;
        this.min = min;
        this.max = max;
    }
    
    public static EloEnum getByRANK(int rank) {
        for(EloEnum elo : values()) {
            if(elo.rank.equals(rank)) {
            	return elo;
            }
        }
        return null;
    }
    
    public static EloEnum getByName(String name) {
        for(EloEnum elo : values()) {
            if(elo.name.equalsIgnoreCase(name)) {
            	return elo;
            }
        }
        return null;
    }
    
    public static EloEnum getbyELO(int rank) {
        for(EloEnum elo : values()) {
            if(elo.min <= rank && rank <= elo.max) {
            	return elo;
            }
        }
        return null;
    }
}
