package com.neziaa.practice.arena;

import java.io.File;

import java.util.ArrayList;

import com.neziaa.practice.Practice;
import com.neziaa.practice.utils.json.DiscUtil;
import com.neziaa.practice.utils.json.JsonPersist;

import lombok.Getter;
import net.minecraft.util.com.google.common.reflect.TypeToken;

/**
 * @author Neziaa
 *  9 dec. 2017
 */

public class ArenaManager implements JsonPersist {
	
	@Getter private ArrayList<Arena> arene = new ArrayList<>();
	
	@Override
	public File getFile() {
		return new File(Practice.getInstance().getDataFolder(), "arena.json");
	}

	@Override
	public void loadData() {
		String content = DiscUtil.readCatch(getFile());
		if (content == null) return;
		
		@SuppressWarnings("serial")
		ArrayList<Arena> map =  JsonPersist.gson.fromJson(content, new TypeToken<ArrayList<Arena>>(){}.getType());
		
		arene.clear();
		arene.addAll(map);
	}

	@Override
	public void saveData(boolean sync) {
		DiscUtil.writeCatch(getFile(), JsonPersist.gson.toJson(arene), true);	
	}
}
