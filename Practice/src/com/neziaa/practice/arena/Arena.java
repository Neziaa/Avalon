package com.neziaa.practice.arena;

import java.util.Arrays;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.neziaa.practice.utils.ItemBuilder;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Neziaa
 *  8 dec. 2017
 */

@Getter @Setter 
public class Arena {
	
	private String name;
	private Location one;
	private Location two;
	private Location spawnAlpha;
	private Location spawnBravo;
	
	private String author;
	
	private boolean used = false;
	
	public Arena(String name, Location one, Location two) {
		this.name = name;
		this.one = one;
		this.two = two;
	}
	
	public ItemStack toItemStack() {
		return new ItemBuilder(Material.SIGN).setName("�7� �9" + name).setLore(Arrays.asList(
				"�8�m-----------------------",
				"�7� Author : �9" + author,
				"�7� World  : �9" + one.getWorld().getName(),
				"�7� Location : �9" + one.getBlockX() + "�7,�9" + one.getBlockY() + "�7,�9" + one.getBlockZ(),
				"�7�                  �9" + two.getBlockX() + "�7,�9" + two.getBlockY() + "�7,�9" + two.getBlockZ(),
				"�7� Utiliser: " + (used == true ? "�aOui" : "�cNon"),
				"�8�m-----------------------")).toItemStack();
	}
}
