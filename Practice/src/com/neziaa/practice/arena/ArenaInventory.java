package com.neziaa.practice.arena;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.neziaa.practice.Practice;
import com.neziaa.practice.utils.ItemBuilder;

/**
 * @author Neziaa
 *  11 dec. 2017
 */

public class ArenaInventory {

private Inventory inv;
	
	public ArenaInventory() {
		this.inv = Bukkit.createInventory(null, 45, "�9Arena");
		updateArena();
		for(int i = 0; i < inv.getSize(); i++) {
			if(inv.getItem(i) == null) {
				inv.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).setName(" ").toItemStack());
			}
		}
	}

	public void updateArena() {
		int i = 1;
		for(Arena arena : Practice.getArenaManager().getArene()) {
			inv.setItem(i, arena.toItemStack());
			i++;
		}
	}
	
	public void open(Player p) {
		p.openInventory(inv);
	}

}
