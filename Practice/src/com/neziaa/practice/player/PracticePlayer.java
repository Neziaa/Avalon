package com.neziaa.practice.player;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import com.neziaa.practice.Practice;
import com.neziaa.practice.duel.DuelRequest;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class PracticePlayer {
	
	private Player player;
	private PlayerState state;
	private Integer elo;
	private ArrayList<DuelRequest> duelRequest = new ArrayList<DuelRequest>();
	
	public PracticePlayer(Player player, PlayerState state, Integer elo) {
		this.player = player;
		this.state = state;
		this.elo = elo;
		this.duelRequest = new ArrayList<DuelRequest>();
	}
	
	public PracticePlayer(Player player) {
		this.player = player;
		this.state = PlayerState.AT_SPAWN;
		this.elo = Practice.getEloManager().getELO(player.getName());
	}

}
