package com.neziaa.practice.player;

import java.io.File;
import java.util.ArrayList;

import com.neziaa.practice.Practice;
import com.neziaa.practice.utils.json.DiscUtil;
import com.neziaa.practice.utils.json.JsonPersist;

import lombok.Getter;
import net.minecraft.util.com.google.common.reflect.TypeToken;

public class PlayerManager implements JsonPersist {
	
	@Getter private ArrayList<PracticePlayer> players = new ArrayList<>();
	
	@Override
	public File getFile() {
		return new File(Practice.getInstance().getDataFolder(), "players.json");
	}

	@Override
	public void loadData() {
		String content = DiscUtil.readCatch(getFile());
		if (content == null) return;
		
		@SuppressWarnings("serial")
		ArrayList<PracticePlayer> map =  JsonPersist.gson.fromJson(content, new TypeToken<ArrayList<PracticePlayer>>(){}.getType());
		
		players.clear();
		players.addAll(map);
	}

	@Override
	public void saveData(boolean sync) {
		DiscUtil.writeCatch(getFile(), JsonPersist.gson.toJson(players), true);	
	}
}
