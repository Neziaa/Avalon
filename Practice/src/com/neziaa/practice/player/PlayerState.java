package com.neziaa.practice.player;

public enum PlayerState {
	
	/**
	 * Player is at spawn / Lobby
	 */
	
	AT_SPAWN,
	
	/**
	 * Player is actually building kit
	 */
	BUILDING_KIT,
	
	/**
	 * Player is actually in Match
	 */
	IN_MATCH,
	
	/**
	 * Player is in FFA World 
	 */
	FFA,
	
	/**
	 * Player is actually spectating a match
	 */
	SPECTATING_MATCH
			

}
